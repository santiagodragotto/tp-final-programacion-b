package models;

import java.io.Serializable;

public class TipoTransaccion implements Serializable {
	
	private String descripcion;
	private boolean esDebito;	
	
	public TipoTransaccion(String descripcion, boolean esDebito) {
		this.descripcion = descripcion;
		this.esDebito = esDebito;
	}
	
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public boolean isEsDebito() {
		return esDebito;
	}

	public void setEsDebito(boolean esDebito) {
		this.esDebito = esDebito;
	}

	@Override
	public String toString() {
		return descripcion + ", Debito: " + (esDebito ? "Si" : "No");
	}

	
}
