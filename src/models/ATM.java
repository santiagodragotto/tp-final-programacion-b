package models;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.math.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Dictionary;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.swing.JOptionPane;

import views.*;

public class ATM implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private int ID;
	private String ubicacion;
	private boolean modoMantenimieto;
	
	private ReconocedorBillete reconocedorBillete;
	private ArrayList<Billetero> billeteros;
	private ArrayList<Banco> bancos;
	private LectorTarjeta lectorTarjeta;
	private ArrayList<Tarifa> tarifas;
	private ArrayList<Cargo> cargos;
	private ArrayList<TipoTransaccion> tiposTransacciones;
	private Banco administrador;
	
	private transient FramePrincipal vistaPrincipal;
	
	public ATM() {
		ID = 0;
		ubicacion = "";
		modoMantenimieto = false;
		reconocedorBillete = new ReconocedorBillete();
		billeteros = new ArrayList<Billetero>();
		bancos = new ArrayList<Banco>();
		lectorTarjeta = new LectorTarjeta();
		tarifas = new ArrayList<Tarifa>();
		tiposTransacciones = new ArrayList<TipoTransaccion>();
		cargos = new ArrayList<Cargo>();
		administrador = new Banco();
	}
	
	public ATM(int ID, String ubicacion, boolean modoMantenimiento) {
		this();
		this.ID = ID;
		this.ubicacion = ubicacion;
		this.modoMantenimieto = modoMantenimiento;
	}
	
	public ATM(int ID, String ubicacion, boolean modoMantenimiento, Banco administrador) {
		this(ID, ubicacion, modoMantenimiento);
		this.administrador = administrador;
	}
	
	public void recargar(BigDecimal valorBillete, int cantidad) {
		Optional<Billetero> billeteroEncontrado = billeteros.stream().filter(p -> p.getValorBillete().equals(valorBillete)).findFirst();
		
		if(billeteroEncontrado.isPresent()) {
			billeteroEncontrado.get().recargar(cantidad);
		}
	}
	
	public void descargar(BigDecimal valorBillete, int cantidad) {
		Optional<Billetero> billeteroEncontrado = billeteros.stream().filter(p -> p.getValorBillete().equals(valorBillete)).findFirst();

		if(billeteroEncontrado.isPresent()) {
			billeteroEncontrado.get().descargar(cantidad);
		}
	}
	
	public void mostrar() {
		vistaPrincipal = new FramePrincipal(this);
	}

	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}

	public String getUbicacion() {
		return ubicacion;
	}

	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}

	public void setBilleteros(ArrayList<Billetero> billeteros) {
		this.billeteros = billeteros;
	}
	
	public boolean isModoMantenimieto() {
		return modoMantenimieto;
	}

	public void setModoMantenimieto(boolean modoMantenimieto) {
		this.modoMantenimieto = modoMantenimieto;
	}

	public Banco getAdministrador() {
		return administrador;
	}

	public void setAdministrador(Banco administrador) {
		this.administrador = administrador;
	}

	public ArrayList<Banco> getBancos() {
		return bancos;
	}
	
	public void addBanco(Banco banco) {
		this.bancos.add(banco);
	}
	
	public void setTarifas(ArrayList<Tarifa> tarifas) {
		this.tarifas = tarifas;
	}
	
	public ArrayList<Tarifa> getTarifas() {
		return this.tarifas;
	}
	
	public void setTiposTransacciones(ArrayList<TipoTransaccion> tiposTransacciones) {
		this.tiposTransacciones = tiposTransacciones;
	}
	
	public ArrayList<TipoTransaccion> getTiposTransacciones() {
		return this.tiposTransacciones;
	}
	
	public void addTipoTransaccion(TipoTransaccion tipoTransaccion) {
		this.tiposTransacciones.add(tipoTransaccion);
	}
	
	public void setCargos(ArrayList<Cargo> cargos) {
		this.cargos = cargos;
	}
	
	public ArrayList<Cargo> getCargos(){
		return this.cargos;
	}
	
	public void addCargo(Cargo cargo) {
		this.cargos.add(cargo);
	}
	
	public Billetero getBilletero(BigDecimal valorBillete) {
		Optional<Billetero> billeteroEncontrado = billeteros.stream().filter(p -> p.getValorBillete().equals(valorBillete)).findFirst();
		
		if(billeteroEncontrado.isPresent()) {
			return billeteroEncontrado.get();
		}
		return null;
	}
	
	public List<TarjetaATM> getAllTarjetas() {
		List<TarjetaATM> tarjetas = new ArrayList<TarjetaATM>();
		for (Banco banco : this.bancos) {
			tarjetas.addAll(banco.getTarjetas());
		}
		return tarjetas;
	}
	
	public boolean validarTarjeta(BigInteger idTarjeta, int PIN) {
		List<TarjetaATM> tarjetas = getAllTarjetas();
		Optional<TarjetaATM> tarjetaEncontrada = tarjetas.stream().filter(p -> p.getID().equals(idTarjeta)).findFirst();
		
		if(tarjetaEncontrada.isPresent()) {
			if(tarjetaEncontrada.get().getPIN() == PIN) {
				return true;
			}
		}
		return false;
	}
	
	public Banco getBancoByTarjeta(TarjetaATM tarjeta) {
		Banco banco = null;
		
		Optional<Banco> bancoFilter = bancos.stream().filter(p -> p.getMinRango().compareTo(tarjeta.getID()) < 0 && p.getMaxRango().compareTo(tarjeta.getID()) > 0).findFirst();
		
		if(bancoFilter.isPresent())
			banco = bancoFilter.get();
		
		return banco;
	}
	
	public List<TarjetaATM> getTarjetasByUsuario(Banco banco, Usuario usuario) {
		ArrayList<TarjetaATM> tarjetas = banco.getTarjetas();
		List<TarjetaATM> tarjetasUsuario = tarjetas.stream().filter(x -> x.getUsuario().equals(usuario)).collect(Collectors.toList());
		
		return tarjetasUsuario;
	}
	
	public Usuario iniciarSesion(String usuario, String password) {
		ArrayList<Usuario> usuarios = this.administrador.getClientes();
		Optional<Usuario> usuariosEncontrado = usuarios.stream().filter(x -> x.getNombreUsuario().equals(usuario)).findFirst();
		
		if(usuariosEncontrado.isPresent()) {			
			Usuario usuarioSelec = usuariosEncontrado.get();
			if(usuarioSelec.getContraseña().equals(password)) {
				return usuarioSelec;
			} else {
				return null;
			}
		} else {
			return null;
		}
	}
	
	public boolean crearUsuario(Banco selectedBanco, String nombre, String apellido, String nombreUsuario, String password) {
		ArrayList<Usuario> usuarios = selectedBanco.getClientes();
		Optional<Usuario> usuariosEncontrado = usuarios.stream().filter(x -> x.getNombreUsuario().equals(nombreUsuario)).findFirst();
		
		if(usuariosEncontrado.isPresent()) {			
			return false;
		} else {
			int maxId = (usuarios.stream().max(Comparator.comparing(Usuario::getID)).get()).getID();
			selectedBanco.agregarUsuario(new Usuario(maxId+1, apellido, nombre, nombreUsuario, password, false));
			return true;
		}
	}
	
	public boolean modificarUsuario(Usuario usuario, Banco selectedBanco, String nombre, String apellido, String nombreUsuario, String password) {
		ArrayList<Usuario> usuarios = selectedBanco.getClientes();
		Optional<Usuario> usuariosEncontrado = usuarios.stream().filter(x -> x.getNombreUsuario().equals(nombreUsuario) && !x.getNombreUsuario().equals(usuario.getNombreUsuario())).findFirst();
		
		if(usuariosEncontrado.isPresent()) {			
			return false;
		} else {
			usuario.setNombre(nombre);
			usuario.setApellido(apellido);
			usuario.setNombreUsuario(nombreUsuario);
			usuario.setContraseña(password);
			return true;
		}
	}
	
	public void serializar() {
		try {
			FileOutputStream fos = new FileOutputStream("atm_data");
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(this);
			oos.close();
			fos.close();
		} catch (IOException ioe) {
            ioe.printStackTrace();
        }
	}
	
	public BigInteger getMaxCBU() {
		BigInteger maxCBU = new BigInteger("0");
		for(Banco banco : bancos) {
			ArrayList<Usuario> usuarios = banco.getClientes();
			for(Usuario usuario : usuarios) {
				if(!(usuario.getCuentas().isEmpty())) {
					BigInteger maxCBUUsuario = (usuario.getCuentas().stream().max(Comparator.comparing(Cuenta::getCBU)).get()).getCBU();
					
					if(maxCBUUsuario.compareTo(maxCBU) > 0) {
						maxCBU = maxCBUUsuario;
					}
				}				
			}
		}
		return maxCBU.add(new BigInteger("1"));
	}
	
	public Cuenta getCuentaByCbu(BigInteger cbu) {
		Cuenta cuenta = null;
		boolean encontro = false;
		int i = 0;
		while(!encontro && i < bancos.size()) {
			ArrayList<Usuario> usuarios = bancos.get(i).getClientes();
			
			int j = 0;
			while(!encontro && j < usuarios.size()) {
				if(!usuarios.get(j).getCuentas().isEmpty()) {
					Optional<Cuenta> cuentaEncontrada = usuarios.get(j).getCuentas().stream().filter(x -> x.getCBU().compareTo(cbu) == 0).findFirst();
					
					if(cuentaEncontrada.isPresent()) {
						cuenta = cuentaEncontrada.get();
						encontro = true;
					}
				}
				j++;
			}
			i++;
		}
		return cuenta; 
	}
	
	public Transaccion depositar(BigInteger cbu, BigDecimal monto, Banco banco) {
		Cuenta cuenta = getCuentaByCbu(cbu);
		Transaccion transaccion = null;
		if(cuenta != null) {
			BigDecimal cargoAplicado;
			if(!banco.equals(this.administrador))
				cargoAplicado = cuenta.aplicarCargoPorDeposito(cargos, false);
			else
				cargoAplicado = cuenta.aplicarCargoPorDeposito(cargos, true);
			
			TipoTransaccion tt = getTipoTransaccionByDescripcion("Deposito");
			cuenta.agregarSaldo(monto.subtract(cargoAplicado));
			transaccion = new Transaccion(monto.subtract(cargoAplicado), tt); 
			cuenta.agregarMovimiento(transaccion);
			
			serializar();
		}
		return transaccion;
	}
	
	public Transaccion extraer(BigInteger cbu, BigDecimal monto) {
		Cuenta cuenta = getCuentaByCbu(cbu);
		Transaccion transaccion = null;
		int[] billetesAExtraer = new int[3]; //0 billetes de 1000, 1 billetes de 500, 2 billetes de 100
		BigDecimal montoAExtraer = new BigDecimal(0);
		if(cuenta != null) {
			if(cuenta.getSaldoParaMovimientos().compareTo(monto) >= 0) {
				billetesAExtraer[0] = 0;
				billetesAExtraer[1] = 0;
				billetesAExtraer[2] = 0;
				int billetes1000 = this.getBilletero(new BigDecimal(1000)).getCantidad();
				int billetes500 = this.getBilletero(new BigDecimal(500)).getCantidad();
				int billetes100 = this.getBilletero(new BigDecimal(100)).getCantidad();

				while(monto.compareTo(new BigDecimal(1000)) >= 0 && billetes1000 > 0) {
					billetesAExtraer[0]++;
					billetes1000--;
					monto = monto.subtract(new BigDecimal(1000));
					montoAExtraer = montoAExtraer.add(new BigDecimal(1000));
				}
				while(monto.compareTo(new BigDecimal(500)) >= 0 && billetes500 > 0) {
					billetesAExtraer[1]++;
					billetes500--;
					monto = monto.subtract(new BigDecimal(500));
					montoAExtraer = montoAExtraer.add(new BigDecimal(500));
				}
				while(monto.compareTo(new BigDecimal(100)) >= 0 && billetes100 > 0) {
					billetesAExtraer[2]++;
					billetes100--;
					monto = monto.subtract(new BigDecimal(100));
					montoAExtraer = montoAExtraer.add(new BigDecimal(100));
				}
				if(monto.compareTo(new BigDecimal(100)) >= 0 && billetes100 == 0) {
					return transaccion;
				} else {
					this.descargar(new BigDecimal(1000), billetesAExtraer[0]);
					this.descargar(new BigDecimal(500), billetesAExtraer[1]);
					this.descargar(new BigDecimal(100), billetesAExtraer[2]);
					
					cuenta.extraerSaldo(montoAExtraer);
					transaccion = new Transaccion(montoAExtraer, getTipoTransaccionByDescripcion("Extraccion"));
					cuenta.agregarMovimiento(transaccion);
				}
			} else {
				return transaccion;
			}
		} else {
			return transaccion;
		}
		serializar();
		return transaccion;
	}
	
	public TipoTransaccion getTipoTransaccionByDescripcion(String descripcion) {
		Optional<TipoTransaccion> tipoTransaccion = this.tiposTransacciones.stream().filter(x -> x.getDescripcion().equals(descripcion)).findFirst();
		if(tipoTransaccion.isPresent())		
			return tipoTransaccion.get();
		else
			return null;
	}
	
	public Usuario getUsuarioByCuenta(Cuenta cuenta) {
		Usuario usuario = null;
		boolean encontro = false;
		int i = 0;
		while(!encontro && i < bancos.size()) {
			ArrayList<Usuario> usuarios = bancos.get(i).getClientes();
			
			int j = 0;
			while(!encontro && j < usuarios.size()) {
				if(!usuarios.get(j).getCuentas().isEmpty()) {
					Optional<Cuenta> cuentaEncontrada = usuarios.get(j).getCuentas().stream().filter(x -> x.getCBU().compareTo(cuenta.getCBU()) == 0).findFirst();
					
					if(cuentaEncontrada.isPresent()) {
						usuario = usuarios.get(j);
						encontro = true;
					}
				}
				j++;
			}
			i++;
		}
		return usuario; 
	}
	
	public void aplicarCargosMensuales() {
		ArrayList<Usuario> clientes = this.administrador.getClientes();
		ArrayList<Cuenta> cuentas = new ArrayList<Cuenta>();
		
		for(Usuario cliente : clientes) {
			cuentas.addAll(cliente.getCuentas());
		}
		
		for (Cuenta cuenta : cuentas) {
			BigDecimal cargoAplicado = cuenta.aplicarCargosMensuales(cargos);
			if(cargoAplicado.compareTo(new BigDecimal(0)) != 0) {
				TipoTransaccion tt = getTipoTransaccionByDescripcion("MantenimientoMensual");
				cuenta.agregarMovimiento(new Transaccion(cargoAplicado, tt));
			}
		}
		serializar();
	}
	
	public void liquidarIntereses() {
		ArrayList<Usuario> clientes = this.administrador.getClientes();
		ArrayList<Cuenta> cuentas = new ArrayList<Cuenta>();
		
		for(Usuario cliente : clientes) {
			cuentas.addAll(cliente.getCuentas());
		}
		
		for (Cuenta cuenta : cuentas) {
			BigDecimal cargoAplicado = cuenta.liquidarIntereses();
			if(cargoAplicado.compareTo(new BigDecimal(0)) != 0) {
				TipoTransaccion tt = getTipoTransaccionByDescripcion("LiqIntereses");
				cuenta.agregarMovimiento(new Transaccion(cargoAplicado, tt));
			}
		}
		serializar();
	}
	
	public Transaccion transferir(Cuenta cuenta, Cuenta cuentaATransferir, BigDecimal saldo) {
		Transaccion transaccion = null;
		if (cuenta.getSaldoParaMovimientos().compareTo(saldo) < 0) {
			return transaccion;
		} else { 
			cuenta.setSaldo(cuenta.getSaldo().subtract(saldo));
			cuentaATransferir.setSaldo(cuentaATransferir.getSaldo().add(saldo));
			
			TipoTransaccion tt = getTipoTransaccionByDescripcion("TransferenciaDesdeOtraCuenta");
			cuentaATransferir.agregarMovimiento(new Transferencia(saldo, tt, cuenta));
			tt = getTipoTransaccionByDescripcion("TransferenciaDesdeOtraCuenta");
			transaccion = new Transferencia(saldo, tt, cuentaATransferir); 
			cuenta.agregarMovimiento(transaccion);
			
			serializar();
			return transaccion;
		}
	}
	
	
}
