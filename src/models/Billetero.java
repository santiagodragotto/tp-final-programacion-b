package models;

import java.io.Serializable;
import java.math.BigDecimal;

public class Billetero implements Serializable {

	private BigDecimal valorBillete;
	private int cantidad;
	
	public void recargar(int cantidad) {
		setCantidad(getCantidad() + cantidad);
	}
	
	public void descargar(int cantidad) {
		setCantidad(getCantidad() - cantidad);
	}
	
	public BigDecimal getValorBillete() {
		return valorBillete;
	}
	
	public void setValorBillete(BigDecimal valorBillete) {
		this.valorBillete = valorBillete;
	}
	
	public int getCantidad() {
		return cantidad;
	}
	
	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	
	public Billetero(BigDecimal valorBillete, int cantidad) {
		this.valorBillete = valorBillete;
		this.cantidad = cantidad;
	}
	
}
