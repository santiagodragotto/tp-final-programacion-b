package models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Optional;

public class Banco implements Serializable {

	private static final int IdTipoCuentaCorriente = 1;
	private static final int IdTipoCuentaAhorro = 2; //enum
	private static final int IdTipoCuentaSueldo= 3;
	
	private String nombre;
	private BigInteger minRango;
	private BigInteger maxRango;
	
	private ArrayList<Usuario> clientes;
	private ArrayList<TarjetaATM> tarjetas;
	
	public Banco() {
		nombre = "";
		minRango = new BigInteger("0");
		maxRango = new BigInteger("0");
		clientes = new ArrayList<Usuario>();
		tarjetas = new ArrayList<TarjetaATM>();
	}
	
	public Banco(String nombre, BigInteger minRango, BigInteger maxRango) {
		this();
		this.nombre = nombre;
		this.minRango = minRango;
		this.maxRango = maxRango;
	}
	
	public void agregarUsuario(Usuario usuario) {
		if(!this.clientes.contains(usuario))
			this.clientes.add(usuario);
	}
	
	public void eliminarUsuario(Usuario usuario) {
		if(this.clientes.contains(usuario))
			this.clientes.remove(usuario);
	}

	public void agregarTarjeta(TarjetaATM tarjeta) {
		if(!this.tarjetas.contains(tarjeta) && (tarjeta.getID().compareTo(minRango) >= 0) && (tarjeta.getID().compareTo(maxRango) <= 0))
			this.tarjetas.add(tarjeta);
	}
	
	public void eliminarTarjeta(TarjetaATM tarjeta) {
		if(this.tarjetas.contains(tarjeta))
			this.tarjetas.remove(tarjeta);
	}
	
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public BigInteger getMinRango() {
		return minRango;
	}

	public void setMinRango(BigInteger minRango) {
		this.minRango = minRango;
	}

	public BigInteger getMaxRango() {
		return maxRango;
	}

	public void setMaxRango(BigInteger maxRango) {
		this.maxRango = maxRango;
	}
	
	public ArrayList<Usuario> getClientes() {
		return clientes;
	}

	public void setClientes(ArrayList<Usuario> clientes) {
		this.clientes = clientes;
	}

	public ArrayList<TarjetaATM> getTarjetas() {
		return tarjetas;
	}

	public void setTarjetas(ArrayList<TarjetaATM> tarjetas) {
		this.tarjetas = tarjetas;
	}
	
	public String toString() {
		return this.nombre;
	}
	
	public void solicitarTarjeta(int pin, Usuario usuario) {
		BigInteger maxId = new BigInteger(this.getMinRango().toString());
		if(!tarjetas.isEmpty())
			maxId = (tarjetas.stream().max(Comparator.comparing(TarjetaATM::getID)).get()).getID();
		
		TarjetaATM tarjeta = new TarjetaATM();
		tarjeta.setID(maxId.add(new BigInteger("1")));
		tarjeta.setUsuario(usuario);
		tarjeta.setPIN(pin);
		
		if(maxId.compareTo(minRango) >= 0 && maxId.compareTo(maxRango) <= 0)
			this.agregarTarjeta(tarjeta);
	}
	
	public void abrirCuenta(BigInteger cbu, int tipoCuenta, String cuitEmpleador, BigDecimal descubierto, Usuario usuario) {
		Optional<Usuario> usuarioEncontrado = clientes.stream().filter(x -> x.getID() == usuario.getID()).findFirst();
		
		if(usuarioEncontrado.isPresent()) {
			Usuario selectedUsuario = usuarioEncontrado.get();
			if(tipoCuenta == IdTipoCuentaAhorro) {
				selectedUsuario.agregarCuenta(new CajaAhorro(cbu, new BigDecimal("0"), new BigDecimal("0"), 0.05, 5000));
			} else {
				if(tipoCuenta == IdTipoCuentaCorriente) {
					selectedUsuario.agregarCuenta(new CuentaCorriente(cbu, new BigDecimal("0"), new BigDecimal("0"), descubierto));
				} else {
					if(tipoCuenta == IdTipoCuentaSueldo) {
						selectedUsuario.agregarCuenta(new CuentaSueldo(cbu, new BigDecimal("0"), new BigDecimal("0"), 0.05, 5000, cuitEmpleador));
					}
				}
			}
		}
		
		
	}
	
}
