package models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;

public class Usuario implements Serializable {

	private int ID;
	private String apellido;
	private String nombre;
	private String contraseña;
	private String nombreUsuario;
	private boolean esAdmin;
	
	private ArrayList<Cuenta> cuentas;
	
	public Usuario() {
		ID = 0;
		apellido = "";
		nombre = "";
		cuentas = new ArrayList<Cuenta>();
	}
	
	public Usuario(int id, String apellido, String nombre, String nombreUsuario, String contraseña, boolean esAdmin) {
		this();
		this.ID = id;
		this.apellido = apellido;
		this.nombre = nombre;
		this.nombreUsuario = nombreUsuario;
		this.contraseña = contraseña;
		this.esAdmin = esAdmin;
	}
	
	public void agregarCuenta(Cuenta cuenta) {
		if(!this.cuentas.contains(cuenta))
			this.cuentas.add(cuenta);
	}
	
	public void eliminarCuenta(Cuenta cuenta) {
		if(this.cuentas.contains(cuenta))
			this.cuentas.remove(cuenta);
	}

	public int getID() {
		return ID;
	}

	public void setID(int iD) {
		ID = iD;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getContraseña() {
		return contraseña;
	}

	public void setContraseña(String contraseña) {
		this.contraseña = contraseña;
	}

	public String getNombreUsuario() {
		return nombreUsuario;
	}

	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}

	public boolean isEsAdmin() {
		return esAdmin;
	}

	public void setEsAdmin(boolean esAdmin) {
		this.esAdmin = esAdmin;
	}

	public ArrayList<Cuenta> getCuentas() {
		return cuentas;
	}

	@Override
	public boolean equals(Object usuario) {
		// TODO Auto-generated method stub
		return this.ID == ((Usuario)usuario).ID;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.apellido + " ," + this.nombre + "; Nombre Usuario: " + this.nombreUsuario;
	}
	
}
