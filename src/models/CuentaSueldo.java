package models;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;

public class CuentaSueldo extends CajaAhorro {

	private String cuitEmpleador;
	
	public CuentaSueldo(BigInteger CBU, BigDecimal saldo, BigDecimal importeDebOtroBanco, double tasaInteres, double limiteExtraccionDia, String cuitEmpleador) {
		super(CBU, saldo, importeDebOtroBanco, tasaInteres, limiteExtraccionDia);
		this.cuitEmpleador = cuitEmpleador;
	}
	
	public String getCuitEmpleador() {
		return cuitEmpleador;
	}

	public void setCuitEmpleador(String cuitEmpleador) {
		this.cuitEmpleador = cuitEmpleador;
	}

	public String toString() {
		return "Cuenta Sueldo: CBU " + this.getCBUFormateado() + ", Cuit Empleador: "  + this.getCuitEmpleador();
	}
	
	public BigDecimal aplicarCargosMensuales(ArrayList<Cargo> cargos) {
		return new BigDecimal(0);
	}
}
