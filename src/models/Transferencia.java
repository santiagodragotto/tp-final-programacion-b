package models;

import java.math.BigDecimal;
import java.util.Date;

public class Transferencia extends Transaccion {

	private Cuenta otraCuenta;
	
	public Transferencia(BigDecimal valor, TipoTransaccion tipoTransaccion, Cuenta otraCuenta) {
		super(valor, tipoTransaccion);
		this.otraCuenta = otraCuenta;
	}
	
	public Transferencia(Date fecha, BigDecimal valor, TipoTransaccion tipoTransaccion, Cuenta otraCuenta) {
		super(fecha, valor, tipoTransaccion);
		this.otraCuenta = otraCuenta;
	}
	
	public String toString() {
		return getFecha() + "\t\t $" + getValor() + "\t\t " + getTipoTransaccion() + "; Cuenta Tercero: CBU: " + otraCuenta.getCBUFormateado();
	}
	
}
