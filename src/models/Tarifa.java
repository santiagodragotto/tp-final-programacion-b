package models;

import java.io.Serializable;
import java.math.BigDecimal;

public class Tarifa implements Serializable {

	private BigDecimal valor;

	private TipoTransaccion tipoTransaccion;
	
	public Tarifa(BigDecimal valor, TipoTransaccion tipoTransaccion) {
		this.valor = valor;	
		this.tipoTransaccion = tipoTransaccion;
	}
	
	public BigDecimal getValor() {
		return this.valor;
	}
	
}
