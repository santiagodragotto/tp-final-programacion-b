package models;

import java.io.Serializable;
import java.math.BigInteger;

public class TarjetaATM implements Serializable {

	private BigInteger ID;
	private int PIN;
	private boolean habilitada;
	
	private Usuario usuario;

	public TarjetaATM() {
		ID = new BigInteger("0");
		PIN = 0;
		habilitada = true;
		usuario = new Usuario();
	}
	
	public TarjetaATM(BigInteger id, int pin, boolean habilitada, Usuario usuario) {
		this.ID = id;
		this.PIN = pin;
		this.habilitada = habilitada;
		this.usuario = usuario;
	}
	
	public BigInteger getID() {
		return ID;
	}

	public void setID(BigInteger iD) {
		ID = iD;
	}

	public int getPIN() {
		return PIN;
	}

	public void setPIN(int pin) {
		PIN = pin;
	}

	public boolean isHabilitada() {
		return habilitada;
	}

	public void setHabilitada(boolean habilitada) {
		this.habilitada = habilitada;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	
	public String toString() {
		return "[" + this.getID() + "]" + this.getUsuario().getApellido() + ", " + this.getUsuario().getNombre();
	}
		
}
