package models;

import java.io.Serializable;

public class Cargo implements Serializable {
	
	private enum TipoMovimiento { Deposito, Extraccion, MantenimientoMensual };
	private enum AplicableA { Movimiento, Cuenta };
	private enum TipoCuenta { CajaAhorroOtroBanco, CuentaCorrienteOtroBanco, CajaAhorroPropioBanco, CuentaCorrientePropioBanco };
	
	private TipoMovimiento tipoMovimiento;
	private AplicableA aplicableA;
	private TipoCuenta tipoCuenta;
	private int aplicableAPartirDelMovimiento;
	private String frecuencia;
	
	private Tarifa tarifa;
	
	public Cargo(String tipoMovimiento, String aplicableA, String frecuencia, String tipoCuenta, int aplicableAPartirDelMovimiento, Tarifa tarifa) {
		this.aplicableAPartirDelMovimiento = aplicableAPartirDelMovimiento;
		this.tarifa = tarifa;
		this.frecuencia = frecuencia;
		
		switch(tipoMovimiento) {
		case "Deposito":
			this.tipoMovimiento = TipoMovimiento.Deposito;
			break;
		case "Extraccion":
			this.tipoMovimiento = TipoMovimiento.Extraccion;
			break;
		case "MantenimientoMensual":
			this.tipoMovimiento = TipoMovimiento.MantenimientoMensual;
			break;
		default:
			this.tipoMovimiento = null;
			break;
		}
		
		this.aplicableA = (aplicableA.compareTo("Movimiento") == 0) ? AplicableA.Movimiento : AplicableA.Cuenta;
		
		switch(tipoCuenta) {
		case "CajaAhorroOtroBanco":
			this.tipoCuenta = TipoCuenta.CajaAhorroOtroBanco;
			break;
		case "CuentaCorrienteOtroBanco":
			this.tipoCuenta = TipoCuenta.CuentaCorrienteOtroBanco;
			break;
		case "CajaAhorroPropioBanco":
			this.tipoCuenta = TipoCuenta.CajaAhorroPropioBanco;
			break;
		case "CuentaCorrientePropioBanco":
			this.tipoCuenta = TipoCuenta.CuentaCorrientePropioBanco;
			break;
		default:
			this.tipoMovimiento = null;
			break;
		}
	}
	
	public String getTipoMovimiento() {
		return this.tipoMovimiento.toString();
	}
	
	public String getTipoCuenta() {
		return this.tipoCuenta.toString();
	}
	
	public Tarifa getTarifa() {
		return this.tarifa;
	}
	
	public int getAplicableAPartirDeMov() {
		return this.aplicableAPartirDelMovimiento;
	}
	
}
