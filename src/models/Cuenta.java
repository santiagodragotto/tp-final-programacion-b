package models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Optional;
import java.util.stream.Stream;

public abstract class Cuenta implements Serializable {

	protected enum TipoMovimiento { Deposito, Extraccion, MantenimientoMensual };
	protected enum TipoCuenta { CajaAhorroOtroBanco, CuentaCorrienteOtroBanco, CajaAhorroPropioBanco, CuentaCorrientePropioBanco };
	
	private BigInteger CBU;
	private BigDecimal saldo;
	private BigDecimal importeDebOtroBanco;
	
	private ArrayList<Transaccion> movimientos;
	
	public Cuenta() {
		CBU = new BigInteger("0");
		saldo = new BigDecimal(0);
		importeDebOtroBanco = new BigDecimal(0);
		movimientos = new ArrayList<Transaccion>();
	}
	
	public Cuenta(BigInteger CBU, BigDecimal saldo, BigDecimal importeDebOtroBanco) {
		this();
		this.CBU = CBU;
		this.saldo = saldo;
		this.importeDebOtroBanco = importeDebOtroBanco;
	}
	
	public void agregarMovimiento(Transaccion transaccion) {
		this.movimientos.add(transaccion);
	}
	
	public void eliminarMovimiento(Transaccion transaccion) {
		this.movimientos.remove(transaccion);
	}

	public BigInteger getCBU() {
		return CBU;
	}

	public void setCBU(BigInteger cBU) {
		CBU = cBU;
	}
	
	public String getCBUFormateado() {
		String CBU = this.getCBU().toString();
		
		while(CBU.length() != 22) {
			CBU = "0" + CBU;
		}
		return CBU;
	}

	public BigDecimal getSaldo() {
		return saldo;
	}

	public BigDecimal getSaldoParaMovimientos() {
		return saldo;
	}
	
	public void setSaldo(BigDecimal saldo) {
		this.saldo = saldo;
	}

	public BigDecimal getImporteDebOtroBanco() {
		return importeDebOtroBanco;
	}

	public void setImporteDebOtroBanco(BigDecimal importeDebOtroBanco) {
		this.importeDebOtroBanco = importeDebOtroBanco;
	}
	
	public void agregarSaldo(BigDecimal monto) {
		this.setSaldo(this.getSaldo().add(monto));
	}
	
	public void extraerSaldo(BigDecimal monto) {
		this.setSaldo(this.getSaldo().subtract(monto));
	}
	
	public String toString() {
		return "CBU " + this.getCBU();
	}
	
	public ArrayList<Transaccion> getMovimientos() {
		return this.movimientos;
	}
	
	public int getCantidadDeMovimientoEnMes(String tipoMovimiento) {
		int cantMovimientos = 0;
		Calendar calendar = Calendar.getInstance();
		Date fechaActual = calendar.getTime();
		SimpleDateFormat fmt = new SimpleDateFormat("yyyyMM");
		
		for (Transaccion movimiento : movimientos) {
			if(fmt.format(movimiento.getFecha()).equals(fmt.format(fechaActual)) && movimiento.getTipoTransaccion().getDescripcion().compareTo(tipoMovimiento) == 0) {
				cantMovimientos++;
			}
		}
		return cantMovimientos;
	}
	
	public abstract BigDecimal liquidarIntereses();
	public abstract BigDecimal aplicarCargosMensuales(ArrayList<Cargo> cargos);
	public abstract BigDecimal aplicarCargoPorDeposito(ArrayList<Cargo> cargos, boolean esBancoAdmin);
		
}
