package models;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Optional;

import models.Cuenta.TipoCuenta;
import models.Cuenta.TipoMovimiento;

public class CajaAhorro extends Cuenta {

	private double tasaInteres;
	private double limiteExtraccionDia;
	
	public CajaAhorro(BigInteger CBU, BigDecimal saldo, BigDecimal importeDebOtroBanco, double tasaInteres, double limiteExtraccionDia) {
		super(CBU, saldo, importeDebOtroBanco);
		this.tasaInteres = tasaInteres;
		this.limiteExtraccionDia = limiteExtraccionDia;
	}
	
	public String toString() {
		return "Caja Ahorro: CBU " + this.getCBUFormateado();
	}
	
	public BigDecimal liquidarIntereses() {
		BigDecimal saldoAAcreditar = this.getSaldo().multiply(new BigDecimal(this.tasaInteres));
		
		this.setSaldo(this.getSaldo().add(saldoAAcreditar));
		return saldoAAcreditar;
	}

	public BigDecimal aplicarCargosMensuales(ArrayList<Cargo> cargos) {
		Optional<Cargo> cargoFilter = cargos.stream().filter(x -> x.getTipoMovimiento().equals(TipoMovimiento.MantenimientoMensual.toString()) && x.getTipoCuenta().equals(TipoCuenta.CajaAhorroPropioBanco.toString())).findFirst();
		
		if(cargoFilter.isPresent()) {
			Cargo cargo = cargoFilter.get();
			
			this.setSaldo(getSaldo().subtract(cargo.getTarifa().getValor()));
			return cargo.getTarifa().getValor();
		}
		return new BigDecimal(0);
	}
	
	public BigDecimal aplicarCargoPorDeposito(ArrayList<Cargo> cargos, boolean esBancoAdmin) {
		Optional<Cargo> cargoFilter;
		if(esBancoAdmin) {
			cargoFilter = cargos.stream().filter(x -> x.getTipoMovimiento().equals(TipoMovimiento.Deposito.toString()) && x.getTipoCuenta().equals(TipoCuenta.CajaAhorroPropioBanco.toString())).findFirst();
			
			if(cargoFilter.isPresent()) {
				Cargo cargo = cargoFilter.get();
				int cantMovMes = this.getCantidadDeMovimientoEnMes("Deposito");
				int aplicableAMov = cargo.getAplicableAPartirDeMov();
				
				if(cantMovMes >= aplicableAMov)
					return cargo.getTarifa().getValor();
			}
			
		} else {
			cargoFilter = cargos.stream().filter(x -> x.getTipoMovimiento().equals(TipoMovimiento.Deposito.toString()) && x.getTipoCuenta().equals(TipoCuenta.CajaAhorroOtroBanco.toString())).findFirst();
			
			if(cargoFilter.isPresent()) {
				return cargoFilter.get().getTarifa().getValor();
			}
		}
		return new BigDecimal(0);
	}
	
}
