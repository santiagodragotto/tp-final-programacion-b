package models;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Optional;

public class CuentaCorriente extends Cuenta {

	private BigDecimal descubierto;
	
	public CuentaCorriente() {
		super();
		descubierto = new BigDecimal(0);
	}
	
	public CuentaCorriente(BigInteger CBU, BigDecimal saldo, BigDecimal importeDebOtroBanco, BigDecimal descubierto) {
		super(CBU, saldo, importeDebOtroBanco);
		this.descubierto = descubierto;
	}

	public BigDecimal getDescubierto() {
		return descubierto;
	}

	public void setDescubierto(BigDecimal descubierto) {
		this.descubierto = descubierto;
	}
	
	@Override
	public BigDecimal getSaldoParaMovimientos() {
		return this.getSaldo().add(this.descubierto);
	}
	
	public String toString() {
		return "Cuenta Corriente: CBU " + this.getCBUFormateado() + "; Descubierto: " + this.descubierto.toString();
	}
	
	public BigDecimal liquidarIntereses() {
		return new BigDecimal(0);
	}
	
	public BigDecimal aplicarCargosMensuales(ArrayList<Cargo> cargos) {
		Optional<Cargo> cargoFilter = cargos.stream().filter(x -> x.getTipoMovimiento().equals(TipoMovimiento.MantenimientoMensual.toString()) && x.getTipoCuenta().equals(TipoCuenta.CuentaCorrientePropioBanco.toString())).findFirst();
		
		if(cargoFilter.isPresent()) {
			Cargo cargo = cargoFilter.get();
			
			this.setSaldo(getSaldo().subtract(cargo.getTarifa().getValor()));
			return cargo.getTarifa().getValor();
		}
		return new BigDecimal(0);
	}
	
	public BigDecimal aplicarCargoPorDeposito(ArrayList<Cargo> cargos, boolean esBancoAdmin) {
		Optional<Cargo> cargoFilter;
		if(esBancoAdmin) {
			cargoFilter = cargos.stream().filter(x -> x.getTipoMovimiento().equals(TipoMovimiento.Deposito.toString()) && x.getTipoCuenta().equals(TipoCuenta.CuentaCorrientePropioBanco.toString())).findFirst();
			
			if(cargoFilter.isPresent()) {
				Cargo cargo = cargoFilter.get();
				int cantMovMes = this.getCantidadDeMovimientoEnMes("Deposito") + 1;
				int aplicableAMov = cargo.getAplicableAPartirDeMov();
				
				if(cantMovMes >= aplicableAMov)
					return cargo.getTarifa().getValor();
			}
			
		} else {
			cargoFilter = cargos.stream().filter(x -> x.getTipoMovimiento().equals(TipoMovimiento.Deposito.toString()) && x.getTipoCuenta().equals(TipoCuenta.CuentaCorrienteOtroBanco.toString())).findFirst();
			
			if(cargoFilter.isPresent()) {
				return cargoFilter.get().getTarifa().getValor();
			}
		}
		return new BigDecimal(0);
	}
	
}
