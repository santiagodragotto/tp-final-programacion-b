package models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;

public class Transaccion implements Serializable {

	private Date fecha;
	private BigDecimal valor;
	
	private TipoTransaccion tipoTransaccion;

	public Transaccion(BigDecimal valor, TipoTransaccion tipoTransaccion) {
		this.valor = valor;
		this.tipoTransaccion = tipoTransaccion;
		this.fecha = Calendar.getInstance().getTime();
	}
	
	public Transaccion(Date fecha, BigDecimal valor, TipoTransaccion tipoTransaccion) {
		this.fecha = fecha;
		this.valor = valor;
		this.tipoTransaccion = tipoTransaccion;
	}
	
	public Date getFecha() {
		return this.fecha;
	}
	
	public TipoTransaccion getTipoTransaccion() {
		return this.tipoTransaccion;
	}
	
	public BigDecimal getValor() {
		return this.valor;
	}
	
	@Override
	public String toString() {
		return  fecha + "\t\t $" + valor + "\t\t " + tipoTransaccion;
	}

	public int compareTo(Transaccion o) {
		return getFecha().compareTo(o.getFecha());
    }
}
