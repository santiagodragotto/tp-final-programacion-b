package views;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;

import models.TarjetaATM;

public class VistaPIN extends StandardPanel {

	protected JPanel panel;
	protected String pin = "";
	protected JLabel titulo;
	private JButton button1;
	private JButton button2;
	private JButton button3;
	private JButton button4;
	private JButton button5;
	private JButton button6;
	private JButton button7;
	private JButton button8;
	private JButton button9;
	private JButton button0;
	protected JLabel lblPIN;
	protected JButton btnVolver;
	private JButton btnLimpiar;
	protected JButton btnIngresar;
	
	public VistaPIN(FramePrincipal framePrincipal) {
		super(framePrincipal);
		initialize();
	}
	
	private void initialize() {
		panel = new JPanel();
		panel.setBounds(0, 0, 800, 500);
		panel.setBackground(Color.WHITE);
		framePrincipal.cargarPanel(panel);
		
		titulo = new JLabel("Ingrese PIN");
		titulo.setFont(new Font("Tahoma", Font.BOLD, 20));
		
		button1 = new JButton("1");
		button1.setFont(new Font("Tahoma", Font.BOLD, 16));
		button1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ingresaNumero(1);
			}
		});
		
		button2 = new JButton("2");
		button2.setFont(new Font("Tahoma", Font.BOLD, 16));
		button2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ingresaNumero(2);
			}
		});
		
		button3 = new JButton("3");
		button3.setFont(new Font("Tahoma", Font.BOLD, 16));
		button3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ingresaNumero(3);
			}
		});
		
		button4 = new JButton("4");
		button4.setFont(new Font("Tahoma", Font.BOLD, 16));
		button4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ingresaNumero(4);
			}
		});
		
		button5 = new JButton("5");
		button5.setFont(new Font("Tahoma", Font.BOLD, 16));
		button5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ingresaNumero(5);
			}
		});
		
		button6 = new JButton("6");
		button6.setFont(new Font("Tahoma", Font.BOLD, 16));
		button6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ingresaNumero(6);
			}
		});
		
		button7 = new JButton("7");
		button7.setFont(new Font("Tahoma", Font.BOLD, 16));
		button7.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ingresaNumero(7);
			}
		});
		
		button8 = new JButton("8");
		button8.setFont(new Font("Tahoma", Font.BOLD, 16));
		button8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ingresaNumero(8);
			}
		});
		
		button9 = new JButton("9");
		button9.setFont(new Font("Tahoma", Font.BOLD, 16));
		button9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ingresaNumero(9);
			}
		});
		
		button0 = new JButton("0");
		button0.setFont(new Font("Tahoma", Font.BOLD, 16));
		button0.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ingresaNumero(0);
			}
		});
		
		lblPIN = new JLabel("");
		lblPIN.setHorizontalAlignment(SwingConstants.CENTER);
		lblPIN.setFont(new Font("Tahoma", Font.BOLD, 18));
		
		btnVolver = new JButton("VOLVER");
		btnVolver.setForeground(new Color(255, 255, 255));
		btnVolver.setBackground(new Color(250, 128, 114));
		
		btnLimpiar = new JButton("LIMPIAR");
		btnLimpiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				pin = "";
				lblPIN.setText("");
			}
		});
		btnLimpiar.setForeground(new Color(255, 255, 255));
		btnLimpiar.setBackground(new Color(255, 165, 0));
		
		btnIngresar = new JButton("INGRESAR");
		btnIngresar.setForeground(new Color(255, 255, 255));
		btnIngresar.setBackground(new Color(0, 128, 0));
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel.createSequentialGroup()
							.addGap(336)
							.addComponent(titulo))
						.addGroup(gl_panel.createSequentialGroup()
							.addGap(329)
							.addGroup(gl_panel.createParallelGroup(Alignment.LEADING, false)
								.addComponent(lblPIN, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addGroup(gl_panel.createSequentialGroup()
									.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING)
										.addComponent(button7)
										.addComponent(button1)
										.addComponent(button4))
									.addPreferredGap(ComponentPlacement.RELATED)
									.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
										.addGroup(gl_panel.createSequentialGroup()
											.addComponent(button5)
											.addPreferredGap(ComponentPlacement.RELATED)
											.addComponent(button6))
										.addGroup(gl_panel.createSequentialGroup()
											.addComponent(button8)
											.addPreferredGap(ComponentPlacement.RELATED)
											.addComponent(button9))
										.addComponent(button0)
										.addGroup(gl_panel.createSequentialGroup()
											.addComponent(button2)
											.addPreferredGap(ComponentPlacement.RELATED)
											.addComponent(button3)))))))
					.addContainerGap(330, Short.MAX_VALUE))
				.addGroup(gl_panel.createSequentialGroup()
					.addGap(308)
					.addComponent(btnLimpiar, GroupLayout.PREFERRED_SIZE, 86, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnIngresar)
					.addContainerGap(313, Short.MAX_VALUE))
				.addGroup(Alignment.TRAILING, gl_panel.createSequentialGroup()
					.addContainerGap(719, Short.MAX_VALUE)
					.addComponent(btnVolver)
					.addContainerGap())
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGap(40)
					.addComponent(titulo)
					.addGap(18)
					.addComponent(lblPIN, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(button1)
						.addComponent(button2)
						.addComponent(button3))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(button4)
						.addComponent(button5)
						.addComponent(button6))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(button7)
						.addComponent(button8)
						.addComponent(button9))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(button0)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnLimpiar)
						.addComponent(btnIngresar))
					.addGap(146)
					.addComponent(btnVolver)
					.addContainerGap(49, Short.MAX_VALUE))
		);
		panel.setLayout(gl_panel);
	}
	
	private void ingresaNumero(int num) {
		if(pin.length() != 4) {
			pin += num;
			lblPIN.setText(lblPIN.getText() + "*");
		}
	}
	
}
