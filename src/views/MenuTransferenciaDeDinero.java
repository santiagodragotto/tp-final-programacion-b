package views;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import models.Banco;
import models.Cuenta;
import models.TarjetaATM;
import models.TipoTransaccion;
import models.Transaccion;
import models.Usuario;

import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.Date;
import java.awt.event.ActionEvent;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;

public class MenuTransferenciaDeDinero extends StandardPanel{

	private JPanel panel;
	private Banco banco;
	private TarjetaATM tarjeta;
	private Cuenta cuenta;
	private Cuenta cuentaATransferir;
	private JTextField textField_CBU;
	private JTextField textField_Usuario;
	private JTextField textField_Monto;

	public MenuTransferenciaDeDinero(FramePrincipal framePrincipal, Banco banco, TarjetaATM tarjeta, Cuenta cuenta) {
		super(framePrincipal);
		this.banco = banco;
		this.tarjeta = tarjeta;
		this.cuenta = cuenta;
		this.cuentaATransferir = null;
		initialize();		
	}
	
	private void initialize() {
		panel = new JPanel();
		panel.setBounds(0, 0, 800, 500);
		panel.setBackground(Color.WHITE);
		framePrincipal.cargarPanel(panel);
		
		JButton btnVolver = new JButton("VOLVER");
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				framePrincipal.removerPanel(panel);
				new MenuOperaciones(framePrincipal, banco, tarjeta, cuenta);
			}
		});
		
		JButton btnConfirmarTransferencia = new JButton("CONFIRMAR TRANSFERENCIA");
		btnConfirmarTransferencia.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				transferir(cuentaATransferir);
			}
		});
		btnConfirmarTransferencia.setEnabled(false);
		
		btnVolver.setForeground(new Color(255, 255, 255));
		btnVolver.setBackground(new Color(250, 128, 114));
		
		textField_CBU = new JTextField();
		textField_CBU.setColumns(10);
		
		JLabel lblTransferenciasAOtras = new JLabel("TRANSFERENCIAS A OTRAS CUENTAS");
		lblTransferenciasAOtras.setFont(new Font("Tahoma", Font.BOLD, 18));
		
		JLabel lblIngreseElCbu = new JLabel("INGRESE EL CBU DE LA CUENTA A LA QUE DESEA TRANSFERIR EL DINERO");
		lblIngreseElCbu.setFont(new Font("Tahoma", Font.BOLD, 12));
		
		JButton btnConfirmar = new JButton("CONFIRMAR");
		btnConfirmar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					BigInteger cbu = new BigInteger(textField_CBU.getText());
					cuentaATransferir = framePrincipal.atm.getCuentaByCbu(cbu);
					
					if(cuentaATransferir != null) {
						Usuario usuario = framePrincipal.atm.getUsuarioByCuenta(cuentaATransferir);
						textField_CBU.setEditable(false);
						btnConfirmar.setEnabled(false);
						textField_Usuario.setText(usuario.toString());
						textField_Monto.setEnabled(true);
						btnConfirmarTransferencia.setEnabled(true);		
					} else {
						JOptionPane.showMessageDialog(null, "No se encontro cuenta con este cbu.");
					}
				} catch(NumberFormatException exc) {
					JOptionPane.showMessageDialog(null, "Debe ingresar un cbu valido");
				}
			}
		});
		
		JLabel lblUsuario = new JLabel("USUARIO:");
		lblUsuario.setFont(new Font("Tahoma", Font.BOLD, 12));
		
		textField_Usuario = new JTextField();
		textField_Usuario.setEditable(false);
		textField_Usuario.setColumns(10);
		
		JLabel lblMontoATransferir = new JLabel("MONTO A TRANSFERIR:");
		lblMontoATransferir.setFont(new Font("Tahoma", Font.BOLD, 12));
		
		textField_Monto = new JTextField();
		textField_Monto.setEnabled(false);
		textField_Monto.setColumns(10);
		
		
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGap(18)
					.addComponent(lblTransferenciasAOtras)
					.addContainerGap(446, Short.MAX_VALUE))
				.addGroup(gl_panel.createSequentialGroup()
					.addGap(33)
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel.createSequentialGroup()
							.addComponent(lblUsuario)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(textField_Usuario, 442, 442, 442))
						.addComponent(btnConfirmarTransferencia)
						.addComponent(lblIngreseElCbu)
						.addGroup(gl_panel.createSequentialGroup()
							.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING, false)
								.addGroup(gl_panel.createSequentialGroup()
									.addComponent(lblMontoATransferir)
									.addGap(28)
									.addComponent(textField_Monto))
								.addComponent(textField_CBU, Alignment.LEADING, GroupLayout.PREFERRED_SIZE, 359, GroupLayout.PREFERRED_SIZE))
							.addGap(54)
							.addComponent(btnConfirmar)))
					.addContainerGap(261, Short.MAX_VALUE))
				.addGroup(Alignment.TRAILING, gl_panel.createSequentialGroup()
					.addContainerGap(683, Short.MAX_VALUE)
					.addComponent(btnVolver)
					.addGap(46))
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblTransferenciasAOtras)
					.addGap(34)
					.addComponent(lblIngreseElCbu)
					.addGap(18)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(textField_CBU, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(btnConfirmar))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblUsuario)
						.addComponent(textField_Usuario, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(81)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblMontoATransferir)
						.addComponent(textField_Monto, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(56)
					.addComponent(btnConfirmarTransferencia)
					.addPreferredGap(ComponentPlacement.RELATED, 89, Short.MAX_VALUE)
					.addComponent(btnVolver)
					.addGap(59))
		);
		panel.setLayout(gl_panel);
	}
	
	private void transferir(Cuenta cuentaATransferir) {
		try {
			BigDecimal saldo = new BigDecimal(textField_Monto.getText());
			Transaccion transaccion = framePrincipal.atm.transferir(cuenta, cuentaATransferir, saldo);
			if (transaccion == null) {
				JOptionPane.showMessageDialog(null, "No se pudo realizar la transferencia");
			} else {
				JOptionPane.showMessageDialog(null, "Transferencia realizada");
				framePrincipal.removerPanel(panel);
				new VistaTicket(framePrincipal, banco, tarjeta, cuenta, transaccion);
			}
		} catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(null, "Debe ingresar un monto v�lido");
		}
	}
}
