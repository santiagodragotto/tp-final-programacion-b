package views;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import models.Banco;
import models.TarjetaATM;

public class CambiarClave extends VistaPIN {

	private Banco banco;
	private TarjetaATM tarjeta;

	public CambiarClave(FramePrincipal framePrincipal, Banco banco, TarjetaATM tarjeta) {
		super(framePrincipal);
		this.banco = banco;
		this.tarjeta = tarjeta;
		initialize();
	}

	private void initialize() {
		titulo.setText("INGRESE CLAVE NUEVA");
		btnIngresar.setText("CAMBIAR");
		
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				framePrincipal.removerPanel(panel);
				new MenuOperaciones(framePrincipal, banco, tarjeta);
			}
		});
		
		btnIngresar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(pin.length() != 4) {
					JOptionPane.showMessageDialog(null, "Debe ser de cuatro digitos");
				} else {
					int PIN = Integer.parseInt(pin);
					tarjeta.setPIN(PIN);
					framePrincipal.removerPanel(panel);
					framePrincipal.atm.serializar();
					new MenuOperaciones(framePrincipal, framePrincipal.atm.getBancoByTarjeta(tarjeta), tarjeta);
				}
			}
		});
	}
	
}
