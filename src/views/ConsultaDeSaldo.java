package views;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import models.Banco;
import models.Cuenta;
import models.TarjetaATM;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ConsultaDeSaldo extends StandardPanel {

	private JPanel panel;
	private Banco banco;
	private TarjetaATM tarjeta;
	private Cuenta cuenta;

	public ConsultaDeSaldo(FramePrincipal framePrincipal, Banco banco, TarjetaATM tarjeta, Cuenta cuenta) {
		super(framePrincipal);
		this.banco = banco;
		this.tarjeta = tarjeta;
		this.cuenta = cuenta;
		initialize();
	}

	private void initialize() {
		panel = new JPanel();
		panel.setBounds(0, 0, 800, 500);
		panel.setBackground(Color.WHITE);
		framePrincipal.cargarPanel(panel);
		
		JLabel lblSaldoActual = new JLabel("SALDO ACTUAL(EN PESOS): ");
		lblSaldoActual.setFont(new Font("Tahoma", Font.BOLD, 15));
		lblSaldoActual.setText(lblSaldoActual.getText() + cuenta.getSaldo().doubleValue());
		
		JButton btnImprimirComprobante = new JButton("IMPRIMIR COMPROBANTE");
		
		JButton btnVolverAlMenu = new JButton("VOLVER");
		btnVolverAlMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				framePrincipal.removerPanel(panel);
				new MenuOperaciones(framePrincipal, banco, tarjeta, cuenta);
			}
		});
		btnVolverAlMenu.setForeground(new Color(255, 255, 255));
		btnVolverAlMenu.setBackground(new Color(250, 128, 114));
		
		JLabel lblConsultaDeSaldo = new JLabel("CONSULTA DE SALDO");
		lblConsultaDeSaldo.setFont(new Font("Tahoma", Font.BOLD, 18));
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addComponent(lblSaldoActual, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 780, Short.MAX_VALUE)
						.addGroup(Alignment.TRAILING, gl_panel.createSequentialGroup()
							.addComponent(btnImprimirComprobante)
							.addPreferredGap(ComponentPlacement.RELATED, 550, Short.MAX_VALUE)
							.addComponent(btnVolverAlMenu))
						.addComponent(lblConsultaDeSaldo))
					.addContainerGap())
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblConsultaDeSaldo)
					.addGap(34)
					.addComponent(lblSaldoActual)
					.addPreferredGap(ComponentPlacement.RELATED, 335, Short.MAX_VALUE)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnVolverAlMenu)
						.addComponent(btnImprimirComprobante))
					.addGap(40))
		);
		panel.setLayout(gl_panel);
	}
	
}
