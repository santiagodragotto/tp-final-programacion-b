package views;

import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JRadioButton;
import javax.swing.LayoutStyle.ComponentPlacement;

import models.Banco;
import models.TarjetaATM;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;

public class CrearCuenta extends StandardPanel {
	
	private static final int IdTipoCuentaCorriente = 1;
	private static final int IdTipoCuentaAhorro = 2;
	private static final int IdTipoCuentaSueldo= 3;

	private JPanel panel;
	private Banco banco;
	private TarjetaATM tarjeta;
	
	private JPanel panelCuentaSueldo;
	
	private final ButtonGroup buttonGroup = new ButtonGroup();
	private JTextField textField;
	private JLabel lblCuitEmpleadorODescubierto;

	public CrearCuenta(FramePrincipal framePrincipal, Banco banco, TarjetaATM tarjeta) {
		super(framePrincipal);
		this.banco = banco;
		this.tarjeta = tarjeta;
		initialize();
	}

	private void initialize() {
		panel = new JPanel();
		panel.setBounds(0, 0, 800, 500);
		panel.setBackground(Color.WHITE);
		framePrincipal.cargarPanel(panel);
		
		JRadioButton rdbtnCuentaSueldo = new JRadioButton("Cuenta Sueldo");
		rdbtnCuentaSueldo.setSelected(true);
		rdbtnCuentaSueldo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblCuitEmpleadorODescubierto.setText("CUIT Empleador:");
				textField.setText("");
				panelCuentaSueldo.setVisible(true);
			}
		});
		buttonGroup.add(rdbtnCuentaSueldo);
		
		JRadioButton rdbtnCajaAhorro = new JRadioButton("Caja Ahorro");
		rdbtnCajaAhorro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelCuentaSueldo.setVisible(false);
			}
		});
		buttonGroup.add(rdbtnCajaAhorro);
		
		JRadioButton rdbtnCuentaCorriente = new JRadioButton("Cuenta Corriente");
		rdbtnCuentaCorriente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				lblCuitEmpleadorODescubierto.setText("Descubierto:");
				textField.setText("");
				panelCuentaSueldo.setVisible(true);
			}
		});
		buttonGroup.add(rdbtnCuentaCorriente);
		
		JLabel lblCrearCuenta = new JLabel("CREAR CUENTA");
		lblCrearCuenta.setFont(new Font("Tahoma", Font.BOLD, 18));
		
		JButton btnVolver = new JButton("VOLVER");
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				framePrincipal.removerPanel(panel);
				new MenuOperaciones(framePrincipal, banco, tarjeta);
			}
		});
		btnVolver.setForeground(new Color(255, 255, 255));
		btnVolver.setBackground(new Color(250, 128, 114));
		
		JLabel lblSeleccioneTipoCuenta = new JLabel("Seleccione tipo cuenta");
		
		JButton btnCrear = new JButton("CREAR");
		btnCrear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					BigInteger cbu = framePrincipal.atm.getMaxCBU();
					String cuitEmpleador = null;
					BigDecimal descubierto = null;
					int tipoCuenta = 0;
					if(rdbtnCajaAhorro.isSelected()) {
						tipoCuenta = IdTipoCuentaAhorro;
					} else {
						if(rdbtnCuentaCorriente.isSelected()) {
							descubierto = new BigDecimal(textField.getText());
							tipoCuenta = IdTipoCuentaCorriente;
						} else {
							if(rdbtnCuentaSueldo.isSelected()) {
								tipoCuenta = IdTipoCuentaSueldo;
								cuitEmpleador = textField.getText();
							}
						}
					}
					banco.abrirCuenta(cbu, tipoCuenta, cuitEmpleador, descubierto, tarjeta.getUsuario());
					framePrincipal.atm.serializar();
					framePrincipal.removerPanel(panel);
					new MenuOperaciones(framePrincipal, banco, tarjeta);
				} catch (NumberFormatException ex) {
					JOptionPane.showMessageDialog(null, "Ingrese un monto valido");
				}
			}
		});
		btnCrear.setForeground(new Color(255, 255, 255));
		btnCrear.setBackground(new Color(0, 128, 0));
		
		panelCuentaSueldo = new JPanel();
		panelCuentaSueldo.setForeground(new Color(255, 255, 255));
		panelCuentaSueldo.setBackground(new Color(255, 255, 255));
		
		
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel.createSequentialGroup()
							.addComponent(rdbtnCuentaSueldo)
							.addGap(18)
							.addComponent(rdbtnCajaAhorro)
							.addGap(18)
							.addComponent(rdbtnCuentaCorriente)
							.addContainerGap(467, Short.MAX_VALUE))
						.addGroup(gl_panel.createSequentialGroup()
							.addComponent(lblCrearCuenta)
							.addGap(329))
						.addGroup(gl_panel.createSequentialGroup()
							.addComponent(lblSeleccioneTipoCuenta)
							.addContainerGap(683, Short.MAX_VALUE))
						.addGroup(gl_panel.createSequentialGroup()
							.addComponent(btnVolver)
							.addPreferredGap(ComponentPlacement.RELATED, 642, Short.MAX_VALUE)
							.addComponent(btnCrear)
							.addContainerGap())
						.addComponent(panelCuentaSueldo, GroupLayout.PREFERRED_SIZE, 235, GroupLayout.PREFERRED_SIZE)))
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblCrearCuenta)
					.addGap(15)
					.addComponent(lblSeleccioneTipoCuenta)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(rdbtnCuentaSueldo)
						.addComponent(rdbtnCajaAhorro)
						.addComponent(rdbtnCuentaCorriente))
					.addGap(18)
					.addComponent(panelCuentaSueldo, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, 267, Short.MAX_VALUE)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnVolver)
						.addComponent(btnCrear))
					.addGap(40))
		);
		
		lblCuitEmpleadorODescubierto = new JLabel("CUIT Empleador");
		
		textField = new JTextField();
		textField.setColumns(10);
		GroupLayout gl_panel_1 = new GroupLayout(panelCuentaSueldo);
		gl_panel_1.setHorizontalGroup(
			gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel_1.createParallelGroup(Alignment.LEADING)
						.addComponent(textField, GroupLayout.PREFERRED_SIZE, 142, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblCuitEmpleadorODescubierto))
					.addContainerGap(83, Short.MAX_VALUE))
		);
		gl_panel_1.setVerticalGroup(
			gl_panel_1.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_1.createSequentialGroup()
					.addGap(5)
					.addComponent(lblCuitEmpleadorODescubierto)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(textField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(20, Short.MAX_VALUE))
		);
		panelCuentaSueldo.setLayout(gl_panel_1);
		panel.setLayout(gl_panel);
	}
}
