package views;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.text.NumberFormatter;

import jdk.nashorn.internal.scripts.JO;
import models.Banco;
import models.Cuenta;
import models.TarjetaATM;
import models.TipoTransaccion;
import models.Transaccion;

import javax.swing.JButton;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.JobAttributes;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Date;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.JFormattedTextField;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

public class DepositarDinero extends StandardPanel {

	private JPanel panel;
	private Banco banco;
	private TarjetaATM tarjeta;
	private Cuenta cuenta;
	private JSpinner billetes100;
	private JSpinner billetes500;
	private JSpinner billetes1000;

	public DepositarDinero(FramePrincipal framePrincipal, Banco banco, TarjetaATM tarjeta, Cuenta cuenta) {
		super(framePrincipal);
		this.banco = banco;
		this.tarjeta = tarjeta;
		this.cuenta = cuenta;
		initialize();
	}
	
	private void initialize() {
		panel = new JPanel();
		panel.setBounds(0, 0, 800, 500);
		panel.setBackground(Color.WHITE);
		framePrincipal.cargarPanel(panel);
		
		JButton btnDepostarDineroA = new JButton("DEPOSTAR DINERO");
		btnDepostarDineroA.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				depositar(cuenta.getCBU());
			}
		});
		
		JButton btnVolverAlMenu = new JButton("VOLVER");
		btnVolverAlMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				framePrincipal.removerPanel(panel);
				new MenuOperaciones(framePrincipal, banco, tarjeta, cuenta);
			}
		});
		btnVolverAlMenu.setForeground(new Color(255, 255, 255));
		btnVolverAlMenu.setBackground(new Color(250, 128, 114));
		
		JLabel lblDepositarDinero = new JLabel("DEPOSITAR DINERO");
		lblDepositarDinero.setFont(new Font("Tahoma", Font.BOLD, 18));
		
		JLabel lblDetalleCuenta = new JLabel(cuenta.toString());
		
		JPanel panel_2 = new JPanel();
		panel_2.setBackground(new Color(255, 255, 255));
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING)
						.addGroup(gl_panel.createSequentialGroup()
							.addPreferredGap(ComponentPlacement.RELATED, 709, Short.MAX_VALUE)
							.addComponent(btnVolverAlMenu)
							.addContainerGap())
						.addGroup(gl_panel.createSequentialGroup()
							.addComponent(lblDepositarDinero)
							.addGap(300))
						.addGroup(gl_panel.createSequentialGroup()
							.addComponent(lblDetalleCuenta, GroupLayout.PREFERRED_SIZE, 368, GroupLayout.PREFERRED_SIZE)
							.addContainerGap(422, Short.MAX_VALUE))
						.addComponent(btnDepostarDineroA, Alignment.LEADING)
						.addGroup(gl_panel.createSequentialGroup()
							.addComponent(panel_2, GroupLayout.DEFAULT_SIZE, 780, Short.MAX_VALUE)
							.addContainerGap())))
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblDepositarDinero)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(lblDetalleCuenta, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnDepostarDineroA)
					.addGap(70)
					.addComponent(panel_2, GroupLayout.PREFERRED_SIZE, 263, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(btnVolverAlMenu)
					.addContainerGap(42, Short.MAX_VALUE))
		);
		
		NumberFormat format = NumberFormat.getInstance();
	    NumberFormatter formatter = new NumberFormatter(format);
	    formatter.setValueClass(BigInteger.class);
	    formatter.setMinimum(0);
	    formatter.setMaximum(Long.MAX_VALUE);
	    formatter.setAllowsInvalid(false);
	    formatter.setCommitsOnValidEdit(true);
		
		billetes100 = new JSpinner();
		
		JLabel lblBilletesDe_2 = new JLabel("Billetes de 100");
		
		billetes500 = new JSpinner();
		
		JLabel lblBilletesDe_3 = new JLabel("Billetes de 500");
		
		billetes1000 = new JSpinner();
		
		JLabel lblBilletesDe_4 = new JLabel("Billetes de 1000");
		GroupLayout gl_panel_2 = new GroupLayout(panel_2);
		gl_panel_2.setHorizontalGroup(
			gl_panel_2.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_2.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel_2.createParallelGroup(Alignment.LEADING, false)
						.addComponent(lblBilletesDe_2, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(billetes100, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel_2.createParallelGroup(Alignment.LEADING, false)
						.addComponent(lblBilletesDe_3, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(billetes500, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel_2.createParallelGroup(Alignment.LEADING, false)
						.addComponent(lblBilletesDe_4, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(billetes1000, GroupLayout.PREFERRED_SIZE, 106, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(427, Short.MAX_VALUE))
		);
		gl_panel_2.setVerticalGroup(
			gl_panel_2.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_2.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel_2.createParallelGroup(Alignment.TRAILING, false)
						.addGroup(gl_panel_2.createSequentialGroup()
							.addComponent(lblBilletesDe_4)
							.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
							.addComponent(billetes1000, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_panel_2.createSequentialGroup()
							.addComponent(lblBilletesDe_3)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(billetes500, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
						.addGroup(gl_panel_2.createSequentialGroup()
							.addComponent(lblBilletesDe_2)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(billetes100, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap(212, Short.MAX_VALUE))
		);
		panel_2.setLayout(gl_panel_2);
		panel.setLayout(gl_panel);
	}
	
	private int getMonto() {
		int monto = 0;
		int cant100 = (int)billetes100.getValue();
		int cant500 = (int)billetes500.getValue();
		int cant1000 = (int)billetes1000.getValue();
		monto += cant100 * 100;
		framePrincipal.atm.recargar(new BigDecimal(100), cant100);
		monto += cant500 * 500;
		framePrincipal.atm.recargar(new BigDecimal(500), cant500);
		monto += (int)billetes1000.getValue() * 1000;
		framePrincipal.atm.recargar(new BigDecimal(1000), cant1000);
		return monto;
	}
	
	private void depositar(BigInteger cbu) {
		int monto = getMonto();
		BigDecimal saldo = new BigDecimal(monto);
		Transaccion transaccion = framePrincipal.atm.depositar(cbu, saldo, banco);
		
		JOptionPane.showMessageDialog(null, "Saldo depositado");
		framePrincipal.removerPanel(panel);
		new VistaTicket(framePrincipal, banco, tarjeta, cuenta, transaccion);
	}
}
