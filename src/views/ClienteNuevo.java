package views;

import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.util.ArrayList;

import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;

import models.Banco;

import javax.swing.JPasswordField;
import javax.swing.JComboBox;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ClienteNuevo extends StandardPanel {

	private JPanel panel;
	private JTextField txtNombre;
	private JTextField txtApellido;
	private JTextField txtNombreUsuario;
	private JPasswordField txtPassword;
	private JPasswordField txtPasswordRepetido;
	
	public ClienteNuevo(FramePrincipal framePrincipal) {
		super(framePrincipal);
		initialize();
	}


	private void initialize() {
		panel = new JPanel();
		panel.setBounds(0, 0, 800, 500);
		panel.setBackground(Color.WHITE);
		framePrincipal.cargarPanel(panel);
		
		JLabel lblNuevoUsuario = new JLabel("NUEVO USUARIO");
		lblNuevoUsuario.setFont(new Font("Tahoma", Font.BOLD, 20));
		
		JLabel lblNombre = new JLabel("NOMBRE");
		
		txtNombre = new JTextField();
		txtNombre.setColumns(10);
		
		JLabel lblApellido = new JLabel("APELLIDO");
		
		txtApellido = new JTextField();
		txtApellido.setColumns(10);
		
		JLabel lblNombreDeUsuario = new JLabel("NOMBRE DE USUARIO");
		
		txtNombreUsuario = new JTextField();
		txtNombreUsuario.setColumns(10);
		
		JLabel lblContrasea = new JLabel("CONTRASE\u00D1A");
		
		txtPassword = new JPasswordField();
		
		JButton btnCrear = new JButton("CREAR");
		btnCrear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				crearUsuario();
			}
		});
		btnCrear.setForeground(new Color(255, 255, 255));
		btnCrear.setBackground(new Color(34, 139, 34));
		
		JButton btnAtras = new JButton("ATRAS");
		btnAtras.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				framePrincipal.removerPanel(panel);
				new Login(framePrincipal);
			}
		});
		btnAtras.setForeground(new Color(255, 255, 255));
		btnAtras.setBackground(new Color(250, 128, 114));
		
		txtPasswordRepetido = new JPasswordField();
		
		JLabel lblValidarContrasea = new JLabel("VALIDAR CONTRASE\u00D1A");
		
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel.createSequentialGroup()
							.addGap(308)
							.addComponent(lblNuevoUsuario))
						.addGroup(gl_panel.createSequentialGroup()
							.addContainerGap()
							.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
								.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING, false)
									.addComponent(txtNombreUsuario, Alignment.LEADING)
									.addComponent(txtNombre, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 378, Short.MAX_VALUE)
									.addComponent(lblNombre, Alignment.LEADING))
								.addComponent(lblNombreDeUsuario))
							.addGap(10)
							.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
								.addComponent(lblApellido)
								.addComponent(txtApellido, GroupLayout.DEFAULT_SIZE, 379, Short.MAX_VALUE)
								.addGroup(gl_panel.createSequentialGroup()
									.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
										.addComponent(txtPassword, GroupLayout.PREFERRED_SIZE, 190, GroupLayout.PREFERRED_SIZE)
										.addComponent(lblContrasea))
									.addPreferredGap(ComponentPlacement.RELATED)
									.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
										.addComponent(lblValidarContrasea)
										.addComponent(txtPasswordRepetido, GroupLayout.DEFAULT_SIZE, 183, Short.MAX_VALUE)))))
						.addGroup(gl_panel.createSequentialGroup()
							.addContainerGap()
							.addComponent(btnAtras)
							.addPreferredGap(ComponentPlacement.RELATED, 635, Short.MAX_VALUE)
							.addComponent(btnCrear)))
					.addGap(23))
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblNuevoUsuario)
					.addGap(39)
					.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING)
						.addComponent(lblApellido)
						.addComponent(lblNombre))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addComponent(txtApellido, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
						.addComponent(txtNombre, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel.createSequentialGroup()
							.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
								.addComponent(lblNombreDeUsuario)
								.addComponent(lblValidarContrasea))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
								.addComponent(txtNombreUsuario, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(txtPassword, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(txtPasswordRepetido, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
						.addComponent(lblContrasea))
					.addPreferredGap(ComponentPlacement.RELATED, 266, Short.MAX_VALUE)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnAtras)
						.addComponent(btnCrear))
					.addGap(38))
		);
		panel.setLayout(gl_panel);
	}
	
	private void crearUsuario() {
		if(validarUsuario()) {
			boolean result = framePrincipal.atm.crearUsuario(framePrincipal.atm.getAdministrador(), txtNombre.getText(), txtApellido.getText(), txtNombreUsuario.getText(), txtPassword.getText());
			if(result) {
				framePrincipal.atm.serializar();
				framePrincipal.removerPanel(panel);
				new Login(framePrincipal);
			} else {
				JOptionPane.showMessageDialog(null, "Nombre de usuario en uso");
			}
		}
	}
	
	private boolean validarUsuario() {
		String mensaje = "";
		boolean valido = true;
		if(txtNombre.getText().length() == 0) {
			mensaje = "Debe ingresar un nombre\n";
			valido = false;
		}
		if(txtApellido.getText().length() == 0) {
			mensaje += "Debe ingresar un apellido\n";
			valido = false;
		}
		if(txtNombreUsuario.getText().length() == 0) {
			mensaje += "Debe ingresar un nombre de usuario\n";
			valido = false;
		}
		if(txtPassword.getText().length() == 0) {
			mensaje += "Debe ingresar una contrase\u00f1a\n";
			valido = false;
		}
		if(txtPasswordRepetido.getText().length() == 0) {
			mensaje += "Debe validar la contrase\u00f1a\n";
			valido = false;
		}
		if(valido && !(txtPassword.getText().equals(txtPasswordRepetido.getText()))) {
			mensaje += "No coinciden las contrase\u00f1as\n";
			valido = false;
		}
		if(!valido) {
			JOptionPane.showMessageDialog(null, mensaje);
		}
		return valido;
	}
}
