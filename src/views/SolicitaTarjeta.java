package views;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import models.Banco;
import models.Usuario;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JPasswordField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.SwingConstants;

public class SolicitaTarjeta extends VistaPIN {

	protected Usuario usuario;

	public SolicitaTarjeta(FramePrincipal framePrincipal, Usuario usuario) {
		super(framePrincipal);
		this.usuario = usuario;
		initialize();
		
	}
	
	private void initialize() {
		btnVolver = new JButton("VOLVER");
		btnVolver.setForeground(new Color(255, 255, 255));
		btnVolver.setBackground(new Color(250, 128, 114));
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				framePrincipal.removerPanel(panel);
				new Tarjetas(framePrincipal);
			}
		});
		
		btnIngresar.setText("SOLICITAR");
		btnIngresar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(pin.length() != 4) {
					JOptionPane.showMessageDialog(null, "Debe ser de cuatro digitos");
				} else {
					int PIN = Integer.parseInt(pin);
					
					framePrincipal.atm.getAdministrador().solicitarTarjeta(PIN, usuario);
					framePrincipal.removerPanel(panel);
					
					new Tarjetas(framePrincipal);
					framePrincipal.atm.serializar();
				}
			}
		});
	}
}
