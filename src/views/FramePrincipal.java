package views;

import java.awt.EventQueue;

import javax.swing.JFrame;

import models.ATM;
import javax.swing.JLabel;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import javax.swing.BoxLayout;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.GridLayout;
import javax.swing.JProgressBar;
import javax.swing.LayoutStyle.ComponentPlacement;

import javafx.scene.control.ProgressBar;
import javax.swing.JPanel;
import java.awt.Color;

public class FramePrincipal {

	protected ATM atm;
	private JFrame frame;
	private JProgressBar progressBar;
	private JLabel lblBienvenidoBanco;
	private JLabel lblCargando;

	public FramePrincipal(ATM atm) {
		this.atm = atm;
		initialize();
	}

	private void initialize() {
		frame = new JFrame();
		frame.setBounds(0, 0, 800, 500);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.WHITE);
		GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addComponent(panel, GroupLayout.DEFAULT_SIZE, 794, Short.MAX_VALUE)
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addComponent(panel, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 471, Short.MAX_VALUE)
		);
		
		progressBar = new JProgressBar();
		progressBar.setValue(0);
		
		lblCargando = new JLabel("CARGANDO");
		
		lblBienvenidoBanco = new JLabel("BIENVENIDO");
		lblBienvenidoBanco.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblBienvenidoBanco.setHorizontalAlignment(SwingConstants.CENTER);
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_panel.createSequentialGroup()
							.addContainerGap()
							.addComponent(progressBar, GroupLayout.DEFAULT_SIZE, 774, Short.MAX_VALUE))
						.addGroup(gl_panel.createSequentialGroup()
							.addContainerGap()
							.addComponent(lblCargando))
						.addGroup(gl_panel.createSequentialGroup()
							.addGap(327)
							.addComponent(lblBienvenidoBanco)))
					.addContainerGap())
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGap(98)
					.addComponent(lblBienvenidoBanco)
					.addGap(118)
					.addComponent(lblCargando)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(progressBar, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE)
					.addContainerGap(183, Short.MAX_VALUE))
		);
		panel.setLayout(gl_panel);
		frame.getContentPane().setLayout(groupLayout);
		frame.setVisible(true);
		cargando();
		removerPanel(panel);
		new Tarjetas(this);
	}
	
	private void cargando() {
		int i = 0;
		
		while(i <= 100) {
			progressBar.setValue(i);
			i += 1;
			if(i == 25) {
				lblCargando.setText("CARGANDO.");
			}
			if(i == 50) {
				lblCargando.setText("CARGANDO..");
			}
			if(i == 75) {
				lblCargando.setText("CARGANDO...");
			}		
			try {
				Thread.sleep(8);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	protected void removerPanel(JPanel panel) {
		frame.getContentPane().remove(panel);
		repintarFrame();
	}
	
	protected void cargarPanel(JPanel panel) {
		frame.getContentPane().add(panel, BorderLayout.NORTH);
		repintarFrame();
	}
	
	private void repintarFrame() {
		frame.revalidate();
		frame.repaint();
	}
	
	
}
