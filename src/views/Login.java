package views;

import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;

import models.*;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JPasswordField;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;

public class Login extends StandardPanel {

	private JPanel panel;
	private JTextField txtUsuario;
	private JPasswordField txtContrasena;

	public Login(FramePrincipal framePrincipal) {
		super(framePrincipal);
		initialize();
	}

	private void initialize() {
		panel = new JPanel();
		panel.setBounds(0, 0, 800, 500);
		panel.setBackground(Color.WHITE);
		framePrincipal.cargarPanel(panel);
		
		JLabel lblIniciarSesion = new JLabel("INICIAR SESION");
		lblIniciarSesion.setFont(new Font("Tahoma", Font.BOLD, 20));		
		JLabel lblUsuario = new JLabel("USUARIO");
		lblUsuario.setFont(new Font("Tahoma", Font.BOLD, 15));
		JLabel lblContrasena = new JLabel("CONTRASE\u00D1A");
		lblContrasena.setFont(new Font("Tahoma", Font.BOLD, 15));
		
		txtUsuario = new JTextField();
		txtUsuario.setColumns(10);
		txtUsuario.addKeyListener(new KeyAdapter() {
	        @Override
	        public void keyPressed(KeyEvent e) {
	            if(e.getKeyCode() == KeyEvent.VK_ENTER){
	            	iniciarSesion();
	            }
	        }

	    });
		
		txtContrasena = new JPasswordField();
		txtContrasena.addKeyListener(new KeyAdapter() {
	        @Override
	        public void keyPressed(KeyEvent e) {
	            if(e.getKeyCode() == KeyEvent.VK_ENTER){
	            	iniciarSesion();
	            }
	        }

	    });
		
		JButton btnCrearUsuario = new JButton("CREAR USUARIO");
		btnCrearUsuario.setForeground(new Color(255, 255, 255));
		btnCrearUsuario.setBackground(new Color(25, 25, 112));
		btnCrearUsuario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				framePrincipal.removerPanel(panel);
				new ClienteNuevo(framePrincipal);
			}
		});
		
		
		JButton btnInciarSesion = new JButton("INICIAR SESION");
		btnInciarSesion.setForeground(new Color(255, 255, 255));
		btnInciarSesion.setBackground(new Color(34, 139, 34));
		btnInciarSesion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				iniciarSesion();
			}
		});
		
		JButton btnVolver = new JButton("VOLVER");
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				framePrincipal.removerPanel(panel);
				new Tarjetas(framePrincipal);
			}
		});
		btnVolver.setBackground(new Color(255, 127, 80));
		btnVolver.setForeground(new Color(255, 255, 255));
		
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addGroup(Alignment.TRAILING, gl_panel.createSequentialGroup()
							.addGroup(gl_panel.createParallelGroup(Alignment.LEADING, false)
								.addComponent(lblContrasena)
								.addComponent(lblUsuario)
								.addComponent(txtUsuario)
								.addComponent(lblIniciarSesion, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(txtContrasena))
							.addGap(313))
						.addGroup(Alignment.TRAILING, gl_panel.createSequentialGroup()
							.addComponent(btnVolver)
							.addPreferredGap(ComponentPlacement.RELATED, 576, Short.MAX_VALUE)
							.addComponent(btnCrearUsuario)
							.addContainerGap())
						.addGroup(Alignment.TRAILING, gl_panel.createSequentialGroup()
							.addComponent(btnInciarSesion, GroupLayout.PREFERRED_SIZE, 132, GroupLayout.PREFERRED_SIZE)
							.addGap(327))))
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblIniciarSesion)
					.addGap(57)
					.addComponent(lblUsuario)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(txtUsuario, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(lblContrasena)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(txtContrasena, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnInciarSesion)
					.addPreferredGap(ComponentPlacement.RELATED, 219, Short.MAX_VALUE)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnCrearUsuario)
						.addComponent(btnVolver))
					.addGap(35))
		);
		panel.setLayout(gl_panel);
	}
	
	private boolean iniciarSesion() {
		boolean valido = true;
		String mensaje = "";
		if(txtUsuario.getText().length() == 0) {
			mensaje = "Ingrese un usuario\n";
			valido = false;
		}
		if(txtContrasena.getText().length() == 0) {
			mensaje += "Ingrese una contraseņa\n";
			valido = false;
		}
		if(!valido) {
			JOptionPane.showMessageDialog(null, mensaje);	
		} else {
			Usuario usuario = framePrincipal.atm.iniciarSesion(txtUsuario.getText(), txtContrasena.getText());
			
			if(usuario != null) {
				framePrincipal.removerPanel(panel);
				new SolicitaTarjeta(framePrincipal, usuario);
			} else {
				JOptionPane.showMessageDialog(null, "Usuario o contraseņa incorrectos");
			}
		}
		return valido;
	}
	
}
