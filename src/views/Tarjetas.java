package views;

import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComboBox;
import javax.swing.LayoutStyle.ComponentPlacement;

import models.*;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.ActionEvent;

public class Tarjetas extends StandardPanel {
	
	private JPanel panel;
		
	public Tarjetas(FramePrincipal framePrincipal) {
		super(framePrincipal);
		initialize();
	}

	private void initialize() {
		panel = new JPanel();
		panel.setBounds(0, 0, 800, 500);
		panel.setBackground(Color.WHITE);
		framePrincipal.cargarPanel(panel);
		
		JLabel lblSeleccioneTarjeta = new JLabel("Seleccione Tarjeta");
		lblSeleccioneTarjeta.setFont(new Font("Tahoma", Font.BOLD, 18));
		
		List<TarjetaATM> tarjetas = framePrincipal.atm.getAllTarjetas();
		JComboBox<TarjetaATM> comboBox = new JComboBox<TarjetaATM>();
		tarjetas.forEach(x -> comboBox.addItem(x));
		
		JButton btnSeleccionar = new JButton("Seleccionar");
		btnSeleccionar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				TarjetaATM tarjeta = (TarjetaATM)comboBox.getSelectedItem();
				if(tarjeta != null) {
					if(tarjeta.isHabilitada()) {
						new PINTarjeta(framePrincipal, tarjeta);
						framePrincipal.removerPanel(panel);
					} else {
						JOptionPane.showMessageDialog(null, "Tarjeta inhabilitada");
					}
					
				} else {
					JOptionPane.showMessageDialog(null, "Seleccione una tarjeta");
				}
			}
		});
		btnSeleccionar.setForeground(new Color(255, 255, 255));
		btnSeleccionar.setBackground(new Color(0, 128, 0));
		
		JButton btnSolicitarTarjeta = new JButton("Solicitar Tarjeta");
		btnSolicitarTarjeta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				framePrincipal.removerPanel(panel);
				new Login(framePrincipal);
			}
		});
		btnSolicitarTarjeta.setForeground(Color.WHITE);
		btnSolicitarTarjeta.setBackground(new Color(0, 128, 128));
		
		JButton btnLiquidarIntereses = new JButton("LIQUIDAR INTERESES");
		btnLiquidarIntereses.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				framePrincipal.atm.liquidarIntereses();
				JOptionPane.showMessageDialog(null, "Se liquidaron los intereses");
			}
		});
		
		JButton btnAplicarCargosMensuales = new JButton("APLICAR CARGOS MENSUALES");
		btnAplicarCargosMensuales.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				framePrincipal.atm.aplicarCargosMensuales();
				JOptionPane.showMessageDialog(null, "Se aplicaron los cargos");
			}
		});
		
		JButton btnVisualizadorBilletero = new JButton("VISUALIZADOR BILLETERO");
		btnVisualizadorBilletero.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				framePrincipal.removerPanel(panel);
				new VisualizadorBilletero(framePrincipal);
			}
		});
		
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addComponent(comboBox, 0, 780, Short.MAX_VALUE)
						.addGroup(gl_panel.createSequentialGroup()
							.addComponent(btnLiquidarIntereses)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnAplicarCargosMensuales)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnVisualizadorBilletero))
						.addGroup(gl_panel.createSequentialGroup()
							.addComponent(btnSeleccionar)
							.addPreferredGap(ComponentPlacement.RELATED, 586, Short.MAX_VALUE)
							.addComponent(btnSolicitarTarjeta))
						.addComponent(lblSeleccioneTarjeta))
					.addContainerGap())
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGap(16)
					.addComponent(lblSeleccioneTarjeta)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnSeleccionar)
						.addComponent(btnSolicitarTarjeta))
					.addPreferredGap(ComponentPlacement.RELATED, 348, Short.MAX_VALUE)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnLiquidarIntereses)
						.addComponent(btnAplicarCargosMensuales)
						.addComponent(btnVisualizadorBilletero))
					.addGap(36))
		);
		panel.setLayout(gl_panel);
	}
}
