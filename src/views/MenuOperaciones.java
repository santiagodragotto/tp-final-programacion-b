package views;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import models.Banco;
import models.Cuenta;
import models.TarjetaATM;
import models.Usuario;

import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.BoxLayout;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.Font;
import javax.swing.JComboBox;

public class MenuOperaciones extends StandardPanel {

	private JPanel panel;
	private Banco banco;
	private TarjetaATM tarjeta;
	private JComboBox<Cuenta> comboBox;
	
	public MenuOperaciones(FramePrincipal framePrincipal, Banco banco, TarjetaATM tarjeta) {
		super(framePrincipal);
		this.banco = banco;
		this.tarjeta = tarjeta;
		initialize(null);
	}
	
	public MenuOperaciones(FramePrincipal framePrincipal, Banco banco, TarjetaATM tarjeta, Cuenta cuenta) {
		super(framePrincipal);
		this.banco = banco;
		this.tarjeta = tarjeta;
		initialize(cuenta);
	}
	
	private void initialize(Cuenta cuenta) {
		boolean esBancoAdmin = framePrincipal.atm.getAdministrador().equals(banco);
		
		panel = new JPanel();
		panel.setBounds(0, 0, 800, 500);
		panel.setBackground(Color.WHITE);
		framePrincipal.cargarPanel(panel);
		
		JButton btnCambioClave = new JButton("Cambio de Clave");
		btnCambioClave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				framePrincipal.removerPanel(panel);
				new MenuCambioDeClave(framePrincipal, banco, tarjeta);
			}
		});
		
		JButton btnDepositar = new JButton("Depositar Dinero");
		btnDepositar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				framePrincipal.removerPanel(panel);
				new DepositarDinero(framePrincipal, banco, tarjeta, (Cuenta)comboBox.getSelectedItem());
			}
		});
		
		JButton btnConsultaSaldo = new JButton("Consulta de Saldo");
		btnConsultaSaldo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				framePrincipal.removerPanel(panel);
				new ConsultaDeSaldo(framePrincipal, banco, tarjeta, (Cuenta)comboBox.getSelectedItem());
			}
		});
		
		JButton btnTransferencia = new JButton("Transferencia de Dinero");
		btnTransferencia.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				framePrincipal.removerPanel(panel);
				new MenuTransferenciaDeDinero(framePrincipal, banco, tarjeta, (Cuenta)comboBox.getSelectedItem());
			}
		});
		
		JButton btnRetirar = new JButton("Retirar dinero");
		btnRetirar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				framePrincipal.removerPanel(panel);
				new MenuRetirardinero(framePrincipal, banco, tarjeta, (Cuenta)comboBox.getSelectedItem());
			}
		});
		
		JButton btnConsultaMov = new JButton("Consulta de Movimientos");
		btnConsultaMov.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				framePrincipal.removerPanel(panel);
				new MenuConsultaDeMovimientos(framePrincipal, banco, tarjeta, (Cuenta)comboBox.getSelectedItem());
			}
		});
		if(!esBancoAdmin)
			btnConsultaMov.setVisible(false);
		
		JButton btnCerrarSesion = new JButton("SALIR");
		btnCerrarSesion.setForeground(new Color(255, 255, 255));
		btnCerrarSesion.setBackground(new Color(250, 128, 114));
		btnCerrarSesion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				framePrincipal.removerPanel(panel);
				new Tarjetas(framePrincipal);
			}
		});
		
		JLabel lblOperaciones = new JLabel("OPERACIONES");
		lblOperaciones.setFont(new Font("Tahoma", Font.BOLD, 18));
		
		ArrayList<Cuenta> cuentas = this.tarjeta.getUsuario().getCuentas();
		comboBox = new JComboBox<Cuenta>();
		cuentas.forEach(x -> comboBox.addItem(x));
		if(cuenta != null) {
			comboBox.setSelectedItem(cuenta);
		}
		
		JLabel lblCuentas = new JLabel("CUENTAS");
		
		JButton btnAbrirCuenta = new JButton("ABRIR CUENTA");
		btnAbrirCuenta.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				framePrincipal.removerPanel(panel);
				new CrearCuenta(framePrincipal, banco, tarjeta);
			}
		});
		btnAbrirCuenta.setForeground(new Color(255, 255, 255));
		btnAbrirCuenta.setBackground(new Color(0, 128, 128));
		
		JButton btnModificarUsuario = new JButton("MODIFICAR USUARIO");
		btnModificarUsuario.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				framePrincipal.removerPanel(panel);
				new ModificaUsuario(framePrincipal, banco, tarjeta);
			}
		});
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING)
						.addGroup(gl_panel.createSequentialGroup()
							.addComponent(lblOperaciones)
							.addGap(332))
						.addGroup(gl_panel.createSequentialGroup()
							.addComponent(comboBox, 0, 780, Short.MAX_VALUE)
							.addContainerGap())
						.addGroup(gl_panel.createSequentialGroup()
							.addComponent(lblCuentas)
							.addContainerGap(744, Short.MAX_VALUE))
						.addGroup(gl_panel.createSequentialGroup()
							.addGroup(gl_panel.createParallelGroup(Alignment.LEADING, false)
								.addComponent(btnCambioClave, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(btnConsultaSaldo, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(btnTransferencia, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(btnDepositar, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(btnRetirar, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(btnConsultaMov, GroupLayout.PREFERRED_SIZE, 176, GroupLayout.PREFERRED_SIZE))
							.addContainerGap(614, Short.MAX_VALUE))
						.addGroup(gl_panel.createSequentialGroup()
							.addComponent(btnAbrirCuenta)
							.addContainerGap())
						.addGroup(gl_panel.createSequentialGroup()
							.addComponent(btnModificarUsuario)
							.addPreferredGap(ComponentPlacement.RELATED, 578, Short.MAX_VALUE)
							.addComponent(btnCerrarSesion)
							.addContainerGap())))
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblOperaciones)
					.addGap(19)
					.addComponent(lblCuentas)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(comboBox, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnAbrirCuenta)
					.addGap(13)
					.addComponent(btnCambioClave)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnRetirar)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnDepositar)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnConsultaSaldo)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnTransferencia)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnConsultaMov)
					.addGap(108)
					.addGroup(gl_panel.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnCerrarSesion)
						.addComponent(btnModificarUsuario))
					.addGap(67))
		);
		panel.setLayout(gl_panel);
	}
}
