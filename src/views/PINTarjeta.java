package views;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;

import models.Banco;
import models.TarjetaATM;
import models.Usuario;

public class PINTarjeta extends VistaPIN {
	
	private TarjetaATM tarjeta;
	private int cantIntentos;
	
	public PINTarjeta(FramePrincipal framePrincipal, TarjetaATM tarjeta) {
		super(framePrincipal);
		this.tarjeta = tarjeta;
		this.cantIntentos = 0;
		initialize();
	}
	
	private void initialize() {
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				framePrincipal.removerPanel(panel);
				new Tarjetas(framePrincipal);
			}
		});
		
		btnIngresar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(pin.length() != 4) {
					JOptionPane.showMessageDialog(null, "Debe ser de cuatro digitos");
				} else {
					int PIN = Integer.parseInt(pin);
					
					boolean result = framePrincipal.atm.validarTarjeta(tarjeta.getID(), PIN);
					if(result) {
						framePrincipal.removerPanel(panel);
						new MenuOperaciones(framePrincipal, framePrincipal.atm.getBancoByTarjeta(tarjeta), tarjeta);
					} else {
						cantIntentos++;
						JOptionPane.showMessageDialog(null, "PIN Invalido, cant intentos restantes: " + (3 - cantIntentos));
						pin = "";
						lblPIN.setText("");
						if(cantIntentos > 2) {
							JOptionPane.showMessageDialog(null, "Tarjeta inhabilitada");
							tarjeta.setHabilitada(false);
							framePrincipal.removerPanel(panel);
							new Tarjetas(framePrincipal);
						}
					}
				}
			}
		});
	}
}
