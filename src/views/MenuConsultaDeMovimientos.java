package views;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import javafx.scene.control.TextArea;
import models.Banco;
import models.Cuenta;
import models.TarjetaATM;
import models.Transaccion;

//import net.miginfocom.swing.MigLayout;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JComboBox;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JTextArea;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;

public class MenuConsultaDeMovimientos extends StandardPanel{

	private JPanel panel;
	private Banco banco;
	private TarjetaATM tarjeta;
	private Cuenta cuenta;

	public MenuConsultaDeMovimientos(FramePrincipal framePrincipal, Banco banco, TarjetaATM tarjeta, Cuenta cuenta) {		
		super(framePrincipal);
		this.banco = banco;
		this.tarjeta = tarjeta;
		this.cuenta = cuenta;
		initialize();
	}
	
	private void initialize() {
		panel = new JPanel();
		panel.setBounds(0, 0, 800, 500);
		panel.setBackground(Color.WHITE);
		framePrincipal.cargarPanel(panel);	
		
		JLabel lblConsultaDeMovimientos = new JLabel("CONSULTA DE MOVIMIENTOS");
		lblConsultaDeMovimientos.setFont(new Font("Tahoma", Font.BOLD, 18));
		
		
		JButton btnVolver = new JButton("VOLVER");
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				framePrincipal.removerPanel(panel);
				new MenuOperaciones(framePrincipal, banco, tarjeta, cuenta);
			}
		});
		btnVolver.setForeground(new Color(255, 255, 255));
		btnVolver.setBackground(new Color(250, 128, 114));
		
		JList<Transaccion> list = new JList<Transaccion>();
		ArrayList<Transaccion> movimientos = cuenta.getMovimientos();
		movimientos.sort((o1,o2) -> o2.getFecha().compareTo(o1.getFecha()));
		list.setListData(movimientos.toArray(new Transaccion[movimientos.size()]));
		
		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGap(23)
					.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING)
						.addGroup(gl_panel.createSequentialGroup()
							.addComponent(lblConsultaDeMovimientos)
							.addContainerGap(508, Short.MAX_VALUE))
						.addGroup(gl_panel.createSequentialGroup()
							.addGroup(gl_panel.createParallelGroup(Alignment.TRAILING)
								.addComponent(list, GroupLayout.DEFAULT_SIZE, 734, Short.MAX_VALUE)
								.addGroup(gl_panel.createSequentialGroup()
									.addPreferredGap(ComponentPlacement.RELATED, 663, Short.MAX_VALUE)
									.addComponent(btnVolver)))
							.addGap(43))))
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addGap(21)
					.addComponent(lblConsultaDeMovimientos)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(list, GroupLayout.PREFERRED_SIZE, 366, GroupLayout.PREFERRED_SIZE)
					.addGap(18)
					.addComponent(btnVolver)
					.addGap(44))
		);
		
		panel.setLayout(gl_panel);
	}
}
