package views;

import java.awt.Color;

import javax.swing.JPanel;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import java.awt.Font;
import java.math.BigDecimal;

import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class VisualizadorBilletero extends StandardPanel {

	private JPanel panel;
	
	public VisualizadorBilletero(FramePrincipal framePrincipal) {
		super(framePrincipal);
		initialize();
	}
	
	private void initialize() {
		panel = new JPanel();
		panel.setBounds(0, 0, 800, 500);
		panel.setBackground(Color.WHITE);
		framePrincipal.cargarPanel(panel);
		
		JLabel lblBilletero = new JLabel("BILLETERO");
		lblBilletero.setFont(new Font("Tahoma", Font.BOLD, 16));
		
		JLabel lblBilletes100 = new JLabel("Billetes 100: ");
		lblBilletes100.setText(lblBilletes100.getText() + framePrincipal.atm.getBilletero(new BigDecimal(100)).getCantidad());
		
		JLabel lblBilletes500 = new JLabel("Billetes 500: ");
		lblBilletes500.setText(lblBilletes500.getText() + framePrincipal.atm.getBilletero(new BigDecimal(500)).getCantidad());
		
		JLabel lblBilletes1000 = new JLabel("Billetes 1000: ");
		lblBilletes1000.setText(lblBilletes1000.getText() + framePrincipal.atm.getBilletero(new BigDecimal(1000)).getCantidad());
		
		JButton btnVolver = new JButton("VOLVER");
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				framePrincipal.removerPanel(panel);
				new Tarjetas(framePrincipal);
			}
		});
		btnVolver.setBackground(new Color(255, 127, 80));
		btnVolver.setForeground(new Color(255, 255, 255));

		GroupLayout gl_panel = new GroupLayout(panel);
		gl_panel.setHorizontalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_panel.createParallelGroup(Alignment.LEADING)
						.addComponent(lblBilletero)
						.addComponent(lblBilletes100)
						.addComponent(lblBilletes1000)
						.addComponent(lblBilletes500))
					.addContainerGap(699, Short.MAX_VALUE))
				.addGroup(Alignment.TRAILING, gl_panel.createSequentialGroup()
					.addContainerGap(719, Short.MAX_VALUE)
					.addComponent(btnVolver)
					.addContainerGap())
		);
		gl_panel.setVerticalGroup(
			gl_panel.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblBilletero)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(lblBilletes100)
					.addGap(40)
					.addComponent(lblBilletes500)
					.addGap(48)
					.addComponent(lblBilletes1000)
					.addPreferredGap(ComponentPlacement.RELATED, 253, Short.MAX_VALUE)
					.addComponent(btnVolver)
					.addGap(52))
		);
		panel.setLayout(gl_panel);
		
	}
}
