package views;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import models.Banco;
import models.Cuenta;
import models.TarjetaATM;
//import net.miginfocom.swing.MigLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import java.awt.Font;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.awt.event.ActionEvent;
import javax.swing.JPasswordField;

public class MenuCambioDeClave extends VistaPIN {
	
	private Banco banco;
	private TarjetaATM tarjeta;

	public MenuCambioDeClave(FramePrincipal framePrincipal, Banco banco, TarjetaATM tarjeta) {
		super(framePrincipal);
		this.banco = banco;
		this.tarjeta = tarjeta;
		initialize();
	}

	private void initialize() {
		titulo.setText("INGRESE CLAVE ACTUAL");
		btnIngresar.setText("CONTINUAR");
		
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				framePrincipal.removerPanel(panel);
				new MenuOperaciones(framePrincipal, banco, tarjeta);
			}
		});
		
		btnIngresar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(pin.length() != 4) {
					JOptionPane.showMessageDialog(null, "Debe ser de cuatro digitos");
				} else {
					int PIN = Integer.parseInt(pin);
					
					boolean result = framePrincipal.atm.validarTarjeta(tarjeta.getID(), PIN);
					if(result) {
						framePrincipal.removerPanel(panel);
						new CambiarClave(framePrincipal, framePrincipal.atm.getBancoByTarjeta(tarjeta), tarjeta);
					} else {
						JOptionPane.showMessageDialog(null, "PIN Invalido");
						pin = "";
						lblPIN.setText("");
					}
				}
			}
		});
	}
}
