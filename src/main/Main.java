package main;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.math.*;
import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import models.*;

public class Main {

	public static void main(String[] args) {
		ATM atm = getData();
		atm.mostrar();
	}
	
	private static ATM getData() {
		try {
			FileInputStream fis = new FileInputStream("atm_data");
			ObjectInputStream ois = new ObjectInputStream(fis);
			
			ATM atm = (ATM)ois.readObject();
			
			ois.close();
			fis.close();
			
			return atm;
		} catch (IOException ioe) {
			ATM atm = setData();
			return atm;
		} catch (ClassNotFoundException c) {
			c.printStackTrace();
			return null;
		}
	}
	
	private static ATM setData() {
		Cuenta cuenta = new CuentaCorriente(new BigInteger("0000000000000000000001"), new BigDecimal(0), new BigDecimal(0), new BigDecimal(2000));
		Usuario usuario = new Usuario(0, "admin", "admin", "admin", "1111", true);
		usuario.agregarCuenta(cuenta);
		usuario.agregarCuenta(new CajaAhorro(new BigInteger("2"), new BigDecimal(1000), new BigDecimal(0), 0.1, 10000));
		usuario.agregarCuenta(new CuentaSueldo(new BigInteger("3"), new BigDecimal(1000), new BigDecimal(0), 0.1, 10000, "20100000000"));
				
		Banco banco = new Banco("Banco de la Plaza", new BigInteger("0"), new BigInteger("999"));
		banco.solicitarTarjeta(1111, usuario);
		banco.agregarUsuario(usuario);
		
		ArrayList<Billetero> billeteros = new ArrayList<Billetero>();
		billeteros.add(new Billetero(new BigDecimal(100), 100));
		billeteros.add(new Billetero(new BigDecimal(500), 100));
		billeteros.add(new Billetero(new BigDecimal(1000), 100));
		
		ATM atm = new ATM(0, "Calle 12, 142", false, banco);
		atm.addBanco(banco);
		atm.setBilleteros(billeteros);
		
		atm.addTipoTransaccion(new TipoTransaccion("Deposito", false));
		atm.addTipoTransaccion(new TipoTransaccion("AcredHaberes", false));
		atm.addTipoTransaccion(new TipoTransaccion("LiqIntereses", false));
		atm.addTipoTransaccion(new TipoTransaccion("TransferenciaDesdeOtraCuenta", false));
		atm.addTipoTransaccion(new TipoTransaccion("Extraccion", true));
		atm.addTipoTransaccion(new TipoTransaccion("TranferenciaAOtraCuenta", true));
		atm.addTipoTransaccion(new TipoTransaccion("CompraEnComercio", true));
		atm.addTipoTransaccion(new TipoTransaccion("CargoClienteOtroBanco", true));
		atm.addTipoTransaccion(new TipoTransaccion("MantenimientoMensual", true));
		
		atm.setTarifas(getTarifas(atm));
		
		try {
			FileOutputStream fos = new FileOutputStream("atm_data");
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(atm);
			oos.close();
			fos.close();
			
			return atm;
		} catch (IOException ioe) {
            ioe.printStackTrace();
            
            return null;
        }
	}
	
	private static ArrayList<Tarifa> getTarifas(ATM atm) {
		
		ArrayList<Tarifa> tarifas = new ArrayList<Tarifa>();
		ArrayList<Cargo> cargos = new ArrayList<Cargo>();
		
		try {
			
			File xmlFile = new File("CargosBancarios.xml");
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(xmlFile);
			
			doc.getDocumentElement().normalize();
			
			NodeList nList = doc.getElementsByTagName("Cargo");
			
			for(int i = 0; i < nList.getLength(); i++) {
				
				Node node = nList.item(i);
				
				if(node.getNodeType() == Node.ELEMENT_NODE) {
					
					Element element = (Element)node;
					
					String tipoMovimiento = element.getElementsByTagName("tipoMovimiento").item(0).getTextContent();
					String aplicableA = element.getElementsByTagName("aplicableA").item(0).getTextContent();
					NodeList nodoFrecuencia = element.getElementsByTagName("frecuencia");

					String frecuencia = null;
					if(nodoFrecuencia.getLength() != 0)
						frecuencia = nodoFrecuencia.item(0).getTextContent();
					
					Element nValores = (Element)element.getElementsByTagName("valores").item(0);
					NodeList nodoValores = nValores.getElementsByTagName("valorPorTipoCuenta");
					
					for(int j = 0; j < nodoValores.getLength(); j++) {
						
						Node nValor = nodoValores.item(j);
						
						Element eValor = (Element)nValor;
						
						String tipoCuenta = eValor.getElementsByTagName("tipoCuenta").item(0).getTextContent();
						int aplicarAPartirDelMovimiento = 0;
						NodeList aplicarAPartirDelMovimientoNodo = eValor.getElementsByTagName("aplicarAPartirDelMovimiento");
						if(aplicarAPartirDelMovimientoNodo.getLength() != 0) {
							aplicarAPartirDelMovimiento = Integer.parseInt(aplicarAPartirDelMovimientoNodo.item(0).getTextContent());
						}
						
						BigDecimal valor = new BigDecimal(eValor.getElementsByTagName("importeADebitar").item(0).getTextContent());
						Tarifa tarifa = new Tarifa(valor, atm.getTipoTransaccionByDescripcion(tipoMovimiento));
						
						tarifas.add(tarifa);
						cargos.add(new Cargo(tipoMovimiento, aplicableA, frecuencia, tipoCuenta, aplicarAPartirDelMovimiento, tarifa));
					}
					
				}
				
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		atm.setCargos(cargos);
		return tarifas;
	}
	
}
